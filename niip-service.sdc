create_clock -name CLOCK_LOC  [get_ports CLOCK_LOC] -period 56MHz
create_clock -name USB_FT_CLKIN  [get_ports USB_FT_CLKIN] -period 60Mhz
#create_clock -name {altera_reserved_tck} {altera_reserved_tck} -period "30.303 ns"
create_clock -name QB_RCLK  [get_ports QB_RCLK] -period 56MHz
create_clock -name CH1_RCLK  [get_ports CH1_RCLK] -period 56MHz
create_clock -name CH2_RCLK  [get_ports CH2_RCLK] -period 56MHz
create_clock -name CH3_RCLK  [get_ports CH3_RCLK] -period 56MHz
create_clock -name CH4_RCLK  [get_ports CH4_RCLK] -period 56MHz
create_clock -name CARU_C_IN  [get_ports CARU_C_IN] -period 56MHz
create_clock -name CYC_SPI_SCK [get_ports CYC_SPI_SCK] -period 700KHz

derive_pll_clocks
derive_clock_uncertainty
set clk28_clock    pllInst|altpll_component|auto_generated|pll1|clk[0]

set_false_path -from [get_registers {*dcfifo*delayed_wrptr_g[*]}] -to [get_registers {*dcfifo*rs_dgwp*}]
set_false_path -from [get_registers {*dcfifo*rdptr_g[*]}] -to [get_registers {*dcfifo*ws_dgrp*}]

set_false_path -from [get_clocks {QB_RCLK}] -to [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path -from [get_clocks {CH1_RCLK}] -to [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path -from [get_clocks {CH2_RCLK}] -to [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path -from [get_clocks {CH3_RCLK}] -to [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path -from [get_clocks {CH4_RCLK}] -to [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path -from [get_clocks {CARU_C_IN}] -to [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}]

set_false_path -from [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}] -to [get_clocks {QB_RCLK}]
set_false_path -from [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}] -to [get_clocks {CH1_RCLK}]
set_false_path -from [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}] -to [get_clocks {CH2_RCLK}]
set_false_path -from [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}] -to [get_clocks {CH3_RCLK}]
set_false_path -from [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}] -to [get_clocks {CH3_RCLK}]

set_false_path -from {UsbProtoParser:UsbProtoParserInst|reqFifoClr}


#set_clock_groups -exclusive -group [get_clocks {USB_FT_CLKIN}] -group [get_clocks {altera_reserved_tck}]  -group [get_clocks {CLOCK_LOC}] -group [get_clocks pllInst|altpll_component|auto_generated|pll1|clk[0]]
#asynchronous 
#set_false_path -from [get_clocks {altera_reserved_tck}] -to [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[0]}]
#set_false_path -from [get_clocks {altera_reserved_tck}] -to [get_clocks {CLOCK_LOC}]
#set_false_path -from {sld_signaltap:auto_signaltap_0|sld_signaltap_impl:sld_signaltap_body|sld_signaltap_implb:sld_signaltap_body|sld_ela_control:ela_control|sld_ela_trigger:\multi_level_advanced_trigger_gen:advanced_trigger_wrapper|sld_ela_trigger_l1q:auto_generated|sld_reserved_niip_service_cyciv_n_auto_signaltap_0_1_80e0:mgl_prim1|lpm_shiftreg:config_shiftreg_106|dffs[2]} -to {sld_signaltap:auto_signaltap_0|sld_signaltap_impl:sld_signaltap_body|sld_signaltap_implb:sld_signaltap_body|sld_ela_control:ela_control|sld_ela_trigger:\multi_level_advanced_trigger_gen:advanced_trigger_wrapper|sld_ela_trigger_l1q:auto_generated|sld_reserved_niip_service_cyciv_n_auto_signaltap_0_1_80e0:mgl_prim1|output283a[0]}

#set_false_path -from {sld_signaltap:auto_signaltap_0|sld_signaltap_impl:sld_signaltap_body|sld_signaltap_implb:sld_signaltap_body|sld_ela_control:ela_control|sld_ela_trigger:\multi_level_advanced_trigger_gen:advanced_trigger_wrapper|sld_ela_trigger_u2q:auto_generated|sld_reserved_niip_service_cyciv_n_auto_signaltap_0_1_77ad:mgl_prim1|LPM_SHIFTREG:config_shiftreg_29|dffs[2]} -to {sld_signaltap:auto_signaltap_0|sld_signaltap_impl:sld_signaltap_body|sld_signaltap_implb:sld_signaltap_body|sld_ela_control:ela_control|sld_ela_trigger:\multi_level_advanced_trigger_gen:advanced_trigger_wrapper|sld_ela_trigger_u2q:auto_generated|sld_reserved_niip_service_cyciv_n_auto_signaltap_0_1_77ad:mgl_prim1|output101a[0]}

#set_false_path  -from [get_clocks {altera_reserved_tck}] -to [get_clocks {CLOCK_LOC}]
#set_false_path -from [get_clocks {USB_FT_CLKIN}] -to [get_clocks {pllInst|altpll_component|auto_generated|pll1|clk[2]}]

#set_false_path -from {QSPI_Master:spiMaster|eBusCh1Usedw[11]} -to {streamProcessor:streamProcessorInst|dataR[3]}
