module sdramController
            (input wire nRst, 
				input wire clk, 

            input [17:0] ebus1FifoQ, ebus2FifoQ,
            input [1:0] ebusFifoEmpty,
            output [1:0] ebusFifoRdReq,
            input eBusReq, reqFifoClr,
            
            input [7:0] pref1,
            input [7:0] pref2,
            
            output [15:0] sdramWrData,
            output reg [15:0] sdramAddr=0,
            output sdramRdReqW, sdramWrReqW,
            input [15:0] sdramRdData,
            input sdramRdValid, sdramWaitReq,
            
            output [7:0] sdramOutFifoQ,
            input sdramFifoRdReq,
            output sdramOutFifoE,
           
            output sdramFull
            );
                                    
wire [15:0] sdramInFifoData;
wire sdramInFifoWrReq, sdramInFifoAF, sdramInFifoF;

reg [5:0] sdramInFifoState=0;
assign sdramInFifoData = sdramInFifoState[0]? {pref1, 6'h0, ebus1FifoQ[17:16]}:
                         sdramInFifoState[1]? ebus1FifoQ[15:0] : 
                         sdramInFifoState[2]? {pref2, 6'h0, ebus2FifoQ[17:16]}: 
                         sdramInFifoState[3]? ebus2FifoQ[15:0] : 16'h0;
assign sdramInFifoWrReq = (sdramInFifoState != 0)&&!sdramInFifoF;

assign ebusFifoRdReq[0] = !sdramInFifoF&&!ebusFifoEmpty[0]&&(!sdramInFifoState || (sdramInFifoState[1]&&ebusFifoEmpty[1]) || sdramInFifoState[3]);
assign ebusFifoRdReq[1] = !sdramInFifoF&&!ebusFifoEmpty[1]&&((!sdramInFifoState&&ebusFifoEmpty[0]) || sdramInFifoState[1] || (sdramInFifoState[3]&&ebusFifoEmpty[0]));

assign sdramFull = (sdramAddr == 15'h7fff);  //32 megs

always  @(posedge clk or negedge nRst) begin
   if(!nRst) begin
      sdramInFifoState <= 0;
   end
   else if(!sdramInFifoF) begin
      if(!sdramInFifoState) begin
         if(!(ebusFifoEmpty[0]))      sdramInFifoState <= 1;
         else if(!(ebusFifoEmpty[1])) sdramInFifoState <= 4;
      end
      else if(sdramInFifoState[0]) sdramInFifoState <= 2;
      else if(sdramInFifoState[1]) 
         sdramInFifoState <= !(ebusFifoEmpty[1]) ? 4 : 
                  !(ebusFifoEmpty[0]) ? 1 : 0;
      
      else if(sdramInFifoState[2]) sdramInFifoState <= 8;
      else if(sdramInFifoState[3])
         sdramInFifoState <= !(ebusFifoEmpty[0]) ? 1 : 
                  !(ebusFifoEmpty[1]) ? 4 : 0;
   end
end

wire [15:0] sdramInFifoQ;
wire sdramInFifoE, sdramInFifoRdReq;
sdramFifo	sdramInFifo (
	.clock ( clk ),
	.data ( sdramInFifoData ),
	.wrreq ( sdramInFifoWrReq ),
   .rdreq ( sdramInFifoRdReq ),
	.almost_full ( sdramInFifoAF ),
	.empty ( sdramInFifoE ),
	.full ( sdramInFifoF ),
	.q ( sdramInFifoQ )
	//.usedw ( usedw_sig )
	);
   
//reg [31:0] ch1DataCount=0; always @(posedge CH1_RCLK) ch1DataCount <= reqFifoClr? 32'h0 : (CH1[17:0] != 18'h001ff)? ch1DataCount+32'h1: ch1DataCount;
//reg [31:0] ch2DataCount=0; always @(posedge CH2_RCLK) ch2DataCount <= reqFifoClr? 32'h0 : (CH2[17:0] != 18'h001ff)? ch2DataCount+32'h1: ch2DataCount;
//reg [31:0] ch3DataCount=0; always @(posedge CH3_RCLK) ch3DataCount <= reqFifoClr? 32'h0 : (CH3[17:0] != 18'h001ff)? ch3DataCount+32'h1: ch3DataCount;
//reg [31:0] ch4DataCount=0; always @(posedge CH4_RCLK) ch4DataCount <= reqFifoClr? 32'h0 : (CH4[17:0] != 18'h001ff)? ch4DataCount+32'h1: ch4DataCount;
//wire strProcEmpty;
//wire [23:0] strProcOut;		
//reg strProcRdReq = 0; always @(posedge CLOCK_56) strProcRdReq<=~strProcEmpty;

//assign strProcEmptyPn = strProcEmpty;
//assign strProcOutPn = strProcOut;
//assign strProcRdReqPn = strProcRd;

//timeOutGenerator timeOutGen(.reset(reset), .clk(CLOCK_56) );
//wire rdReg8w = (~usbFifoFull) && (~strProcEmpty);
//wire [7:0] strProc8Out;

reg [31:0] delay = 100; //10000000;
reg [4:0] sdramState=0;


//reg sdram1_wrReq=0;
//reg [24:0] sdramAddr=0;
//reg [24:0] sdramDataCount=0;
//reg sdramWaitReqR=0; always @(posedge clkInt) sdramWaitReqR <= sdramWaitReq;

//reg [100:0] ebusFifoEmptyRR=0; always @(posedge clkInt) ebusFifoEmptyRR <= {ebusFifoEmptyRR[99:0], !ebusFifoEmpty[0]};
reg [100:0] sdramInFifoERR=0; always @(posedge clk) sdramInFifoERR <= {sdramInFifoERR[99:0], !sdramInFifoE};
assign sdramInFifoRdReq = sdramState[3]? !sdramInFifoE && !sdramWaitReq && !sdramFull :0;
reg sdramInFifoRdReqR=0; always @(posedge clk) if(!sdramWaitReq) sdramInFifoRdReqR <= sdramInFifoRdReq; //!!!
//reg ebusFifoRdReqR=0;  //always @(posedge clkInt) if(!sdramWaitReq) ebusFifoRdReqR <= ebusFifoRdReq[0]; //!!!

/* reg ebusFifoRdReqStateBeforeSdramWaitReq = 0; 
always @(posedge clkInt) begin
   if(sdramWaitReq && !sdramWaitReqR) begin 
      ebusFifoRdReqStateBeforeSdramWaitReq <= ebusFifoRdReqR;
   end
   else if(!sdramWaitReq) begin 
      ebusFifoRdReqStateBeforeSdramWaitReq <= 0;
   end
end */

assign sdramWrReqW = sdramState[0]? !sdramWaitReq && (sdramAddr < 20'hf):
                     sdramState[3]?  !sdramWaitReq && sdramInFifoRdReqR && !sdramFull /*(sdramAddr < 26'h2000000)*/  : 0;
                    
//reg sdram1_wrReqR = 0; always @(posedge clk) sdram1_wrReqR <= sdramWrReqW;




//reg [24:0] sdram2Addr=0;
wire [15:0] sdram2RdData;
wire sdram2RdValid;
reg [15:0] wr_data=0;

assign sdramWrData = sdramState[0]? wr_data:
                           sdramState[3]? sdramInFifoQ: 16'h0;

wire sdramOutFifoAF;
reg [26:0] wordsToRead=0;
assign sdramRdReqW=sdramState[2]&&!sdramWaitReq&&(sdramAddr < wordsToRead) && !sdramOutFifoAF;
//reg [21:0] rd_adr=0;

//reg sdram1Full=0;

reg [1:0] ebusReqR = 0; 
always @(posedge clk) begin
   ebusReqR <= sdramState[2]? 0 : {ebusReqR[0], eBusReq | ebusReqR[0]};
   if(ebusReqR==2'b01) begin
      wordsToRead <= sdramAddr;
   end
      
end
      

always @(posedge clk or negedge nRst) begin
   if(!nRst) begin
      sdramState<=0;
      sdramAddr<=0;
      wr_data<=0;
      delay <= 100;
      //sdramDataCount <= 0;
   end
   else begin
      if(sdramState==0) begin
         if(ebusReqR[0])begin
            sdramAddr <= 0;
            sdramState <= 4;
         end
         else begin
            if(delay==0) begin
               sdramState <= 2;
               //sdram1_wrReq <=1;
               sdramAddr <= 0;
               //wr_data <= 0;
            end
            else begin
               delay <= delay - 1;
            end
         end
      end
      else if(sdramState[0]) begin
         if(!sdramWaitReq) begin
            if(sdramAddr < 20'ha) begin
               sdramAddr <= sdramAddr + 1;
               wr_data <= wr_data + 1;
               //sdram1_wrReq <= 1;
               //sdramDataCount <= sdramDataCount + 1;
            end
            else if(sdramAddr < 20'hf) begin
               sdramAddr <= sdramAddr + 1;
               //sdramDataCount <= sdramDataCount + 1;
               wr_data <= 0;
            end
            else  begin
   /*             if(reqFifoClr) begin
                  rd_adr <= 0;
                  //sdramAddr <= 25'hfff;
                  sdramAddr <= 25'h0;
                  sdram1Full <= 0;
                  sdramState <= 0;
                  delay <= 10000000;
               end
               else begin */
                  //sdramAddr <= 0;
                  //sdramFull <= 1;
                  sdramState <= 2;
                  delay <= 10000000;
                  //sdram1_wrReq <=0;
               //end
            end
            
         end
         else begin
            //sdram1_wrReq <= 0;
         end
      end
      
      else if(sdramState[1]) begin   //IDLE
         if(ebusReqR)begin
            sdramAddr <= 0;
            sdramState <= 4;            
            //sdramDataCount <= sdramAddr;            
         end
         else if(~sdramInFifoE && !sdramFull) begin
            //sdramAddr <= 0;            
            sdramState <= 8;
         end 
         else if(reqFifoClr) begin
            sdramAddr <= 0;
            //sdramDataCount <= 0;
         end
         
         //if(sdramRdValid) begin
         //   sdramAddr <= sdramAddr + 1;
         //end
         //if(sdramAddr == 0) begin
            //sdramState <= 4;  
            //sdram1RdReq <= 1;
            //sdramAddr <= 0;
         //end
         //else begin
            //sdramAddr <= sdramAddr - 1;
         //end
      end
      
      else if(sdramState[2]) begin     //SDRAM read
         if(!sdramWaitReq && !sdramOutFifoAF && (sdramAddr < wordsToRead) ) begin
            sdramAddr <= sdramAddr + 1;
         end
         else begin
            if(sdramAddr == wordsToRead) begin
               sdramState <= 2;
               sdramAddr <= 0;
               //sdramDataCount <= 0;
            end
         end
      end
      
      else if(sdramState[3]) begin     //SDRAM write
         if((sdramInFifoERR == 0) || sdramFull /*(sdramAddr == 26'h2000000)*/ ) 
            sdramState <= 2;
         if(sdramWrReqW) begin 
            sdramAddr <= sdramAddr + 1;
            //sdramDataCount <= sdramDataCount + 1;
         end
      end
   end

end

//wire sdram1_wrGnt;


//reg [31:0] dataCounter=0;


/* assign SDRAM1_A[12:11] = 0;
assign SDRAM1_BA[1] = 0;
SDRAM_ctrl sdram1Ctrl(.clk(CLOCK_PLL_112), 
                  .RdReq(sdram1_rd_req), .RdAddr(rd_adr), .RdData(rd_data), .RdDataValid(RdDataValid),
                  .WrReq(sdram1_wrReq), .WrGnt(sdram1_wrGnt), .WrAddr(wr_adr), .WrData(wr_data),
                  .SDRAM_WEn(SDRAM1_WE), .SDRAM_CASn(SDRAM1_CAS), .SDRAM_RASn(SDRAM1_RAS),
                  .SDRAM_A(SDRAM1_A[10:0]), .SDRAM_BA(SDRAM1_BA[0]), .SDRAM_DQ(SDRAM1_D)); */




/* reg [15:0] dta = 0;
reg sdramFifoWrReq=0;
always @(posedge clkInt) begin
   if(!sdram1FifoAF) begin
      dta <= dta + 1;
      sdramFifoWrReq <= 1;
   end
   else begin
      sdramFifoWrReq <= 0;
   end
   
end */
                  

wire [7:0] sdramOutFifoWrusedw;
assign sdramOutFifoAF = sdramOutFifoWrusedw[7];
wire sdramFifoF;
sdramOutFifo	sdramOutFifo_inst (
   .aclr(reqFifoClr),
	.wrclk(clk),
	.wrreq ( sdramRdValid ),
   .data ( {sdramRdData[7:0], sdramRdData[15:8]}  ),
   //.wrfull ( sdramFifoF ),
	//.almost_full ( sdram1FifoAF ),
   .wrusedw(sdramOutFifoWrusedw),
	
   .rdclk ( clk ),
   .rdreq ( sdramFifoRdReq ),
	.q ( sdramOutFifoQ ),
   .rdempty ( sdramOutFifoE )//,
	//.usedw ( usedw_sig )
	);
   
/* sdramFifo	sdramFifo_inst (
	.clock ( clkInt ),
	.data ( sdram1RdData ),
	.wrreq ( sdramRdValid ),
   .rdreq ( sdram1FifoRdReq ),
	.almost_full ( sdram1FifoAF ),
	.empty ( sdram1FifoE ),
	.full ( sdram1FifoF ),
	.q ( sdram1FifoQ ),
	.usedw ( usedw_sig )
	);
 */

   
endmodule

			