module UsbProtoGenerator(input nrst, input clk, 				
            output inReady,
            input inValid,
            input sop,
            input [7:0] data,
            
            input outReady,
            output fifoWrClk,
				output reg outValid = 0,
				output reg [7:0] q=0,
            output reg prevPacketErr=0);
            

assign fifoWrClk = clk;
//assign wrAllow = !usbFifoFull;

parameter [3:0] IDLE  = 4'd8,
                SEND_MWH = 4'd7,
                SEND_MWL = 4'd6,
                SEND_LEN_H = 4'd5,
                SEND_LEN_L = 4'd4,
                SEND_DATA = 4'd3,
                SEND_CRCH = 4'd2,
                SEND_CRCL = 4'd1,
                SEND_STOP = 4'd0;
reg [3:0] dataSendState=IDLE;

reg [7:0] crcDataR;


reg [15:0] crc16 = 16'h0;

//reg [7:0] crcData = 8'h0;
reg [15:0] crc16tr; // = nextCRC16_D8(crcDataIn, crc16In);
//wire [15:0] crc16In = resetCRC? 16'hffff : crc16tr;
wire [7:0] crcDataIn = {q[0],q[1],q[2],q[3],q[4],q[5],q[6],q[7]};
wire [15:0] crc16t = nextCRC16_D8(crcDataIn, crc16tr);

wire [15:0] crc16Out = {crc16t[0],crc16t[1],crc16t[2],crc16t[3],crc16t[4],crc16t[5],crc16t[6],crc16t[7],crc16t[8],crc16t[9],crc16t[10],crc16t[11],crc16t[12],crc16t[13],crc16t[14],crc16t[15]}^16'hffff;
reg [15:0] crc16r;
reg resetCRC = 0;

reg readyToRecv = 1;
assign inReady = outReady&&readyToRecv;

reg [15:0] dataCount=0;

always @(posedge clk) begin
  
   if(outReady) begin
      //crc16tr <=  resetCRC? 16'hffff : crc16t;
      if(inValid && sop /*&& (dataLen!=0)*/) begin
         dataSendState <= SEND_MWL;
         readyToRecv <= 0;
         //dataCount <= dataLen;
         q <= 8'hbe; //send mw h
         outValid <= 1;                     
         resetCRC <= 1;
         crc16tr <= 16'hffff;
         if(dataSendState != IDLE) begin
            prevPacketErr <= 1;
         end
         else begin
            prevPacketErr <= 0;
         end
      end
      else begin      
         case(dataSendState)
         IDLE: begin
            readyToRecv <= 1;       
         end
   /*       SEND_MWH: begin
            q <= 8'hbe; //send mw h
            outValid <= 1;                     
            //resetCRC <= 1;
         end */
         SEND_MWL: begin
            q <= 8'hef; //send mw l
            resetCRC <= 0;
            dataSendState <= SEND_LEN_H;
            crc16tr <= crc16t;
            readyToRecv <= 1;
         end
         SEND_LEN_H: begin   
            if(inValid) begin         
               q <= data;
               dataCount[15:8] <= data;
               dataSendState <= SEND_LEN_L;
               crc16tr <= crc16t;
            end
         end
         SEND_LEN_L: begin            
            if(inValid) begin                  
               q <= data;
               dataCount[7:0] <= data;
               //dataCount <= dataCount - 1;
               dataSendState <= SEND_DATA;
               crc16tr <= crc16t;
            end
         end
         SEND_DATA: begin 
            if(inValid) begin
               outValid <= 1;
               q <= data;
               if(dataCount>1) begin
                  dataCount <= dataCount-1;
               end
               else begin
                  readyToRecv <= 0;
                  dataSendState <= SEND_CRCH;
               end
               crc16tr <= crc16t;
            end
            else begin
               outValid <= 0;
            end
         end
         SEND_CRCH: begin                
            crc16r <= crc16Out;
            q <= crc16Out[15:8];      
            dataSendState <= SEND_CRCL;         
         end
         SEND_CRCL: begin                     
            q <= crc16r[7:0];
            readyToRecv <= 1;
            dataSendState <= SEND_STOP;
         end
         SEND_STOP: begin
            outValid <= 0;
            dataSendState <= IDLE;
         end               
         endcase
      end
      
   end
   else begin
   end
end

  function [15:0] nextCRC16_D8;

    input [7:0] Data;
    input [15:0] crc;
    reg [7:0] d;
    reg [15:0] c;
    reg [15:0] newcrc;
  begin
    d = Data;
    c = crc;

    newcrc[0] = d[4] ^ d[0] ^ c[8] ^ c[12];
    newcrc[1] = d[5] ^ d[1] ^ c[9] ^ c[13];
    newcrc[2] = d[6] ^ d[2] ^ c[10] ^ c[14];
    newcrc[3] = d[7] ^ d[3] ^ c[11] ^ c[15];
    newcrc[4] = d[4] ^ c[12];
    newcrc[5] = d[5] ^ d[4] ^ d[0] ^ c[8] ^ c[12] ^ c[13];
    newcrc[6] = d[6] ^ d[5] ^ d[1] ^ c[9] ^ c[13] ^ c[14];
    newcrc[7] = d[7] ^ d[6] ^ d[2] ^ c[10] ^ c[14] ^ c[15];
    newcrc[8] = d[7] ^ d[3] ^ c[0] ^ c[11] ^ c[15];
    newcrc[9] = d[4] ^ c[1] ^ c[12];
    newcrc[10] = d[5] ^ c[2] ^ c[13];
    newcrc[11] = d[6] ^ c[3] ^ c[14];
    newcrc[12] = d[7] ^ d[4] ^ d[0] ^ c[4] ^ c[8] ^ c[12] ^ c[15];
    newcrc[13] = d[5] ^ d[1] ^ c[5] ^ c[9] ^ c[13];
    newcrc[14] = d[6] ^ d[2] ^ c[6] ^ c[10] ^ c[14];
    newcrc[15] = d[7] ^ d[3] ^ c[7] ^ c[11] ^ c[15];
    nextCRC16_D8 = newcrc;
  end
  endfunction
  
endmodule

module UsbProtoParser(input nrst, input clk, 
                        input [7:0] data, input fifoEmpty, output fifoRdReq, input [12:0] fifoUsedw,
                        output [23:0] q, 
                        input SCQready, output SCQvalid,
                        input qBusReady, output qBusDataValid, output updateParamsStart, output updateParamsEnd,
                        output reg reqParamsSend=0, output reg sendParamsOnTim2=0,
                        output reg stateReq=0, output reg qBusReq=0, output reg [7:0] eBusReq=8'h0, output reg reqFifoClr=0,
                        output reg caru_en=0, output [3:0] stateW, 
                        output reg eBusTestMode=0);

localparam [3:0] IDLE  = 4'd0,
                W_BE = 4'd1,
                W_EF = 4'd2,
                CHECK = 4'd3,
                R_AD9510 = 4'd4,
                R_QBUS = 4'd5,
                DELAY = 4'd6;
                
localparam [7:0] AD9510  =  8'haa,
                 QBUS =     8'hbb,
                 REQ_STATE = 8'hcc,
                 REQ_QBUS = 8'hdd,
                 REQ_PARAMS_SEND = 8'hde,
                 SEND_PARAMS_ON_TIM2_ON = 8'hda,
                 SEND_PARAMS_ON_TIM2_OFF = 8'hdb,
                 EBUS_TEST_MDOE_ON = 8'hea,  //imit input serdes data ON
                 EBUS_TEST_MDOE_OFF = 8'heb,  //imit input serdes data OFF
                 REQ_EBUS1 = 8'he0,
                 REQ_EBUS2 = 8'he1,
                 REQ_EBUS3 = 8'he2,
                 REQ_EBUS4 = 8'he3,
                 REQ_EBUS5 = 8'he4,
                 REQ_EBUS6 = 8'he5,
                 REQ_EBUS7 = 8'he6,
                 REQ_EBUS8 = 8'he7,
                 REQ_EBUS_ALL = 8'he8,
                 REQ_FIFO_CLR = 8'h99,
                 REQ_CARU_DIS = 8'h80,
                 REQ_CARU_ENA = 8'h81;

reg [3:0] state = IDLE ;
assign stateW = state;
//reg [3:0] rcvCount=0;
reg [15:0] rcvRdReqCount=0;
reg [15:0] fsmByteCount=0;
reg fifoDataValid = 0;
reg fifioRdReqR = 0;
reg [15:0] bCountToRcv = 0;
reg [1:0] recvByteCount=0;
//wire readyToRecv =  (state==R_QBUS)? ((bCountToRcv>0)||(fifoUsedw>=bCountToRcv-1))&&((recvByteCount<2)|| ((recvByteCount==2)&&qBusDataReady&&(bCountToRcv>0)))  /* ((rcvRdReqCount<6)|| (bCountToRcv>0))*/ : 
//                     (rcvRdReqCount<6)  ;
//wire qBusReadyToRecv = (state==R_QBUS) && ((recvByteCount<2) || (((bCountToRcv>0)||(fifoUsedw>=bCountToRcv-1))&&((recvByteCount<2)|| ((recvByteCount==2)&&qBusDataReady&&(bCountToRcv>0)))  /* ((rcvRdReqCount<6)|| (bCountToRcv>0))*/) );
wire qBusReadyToRecv = (state==R_QBUS) && ((fsmByteCount<5) || (bCountToRcv>0)) && qBusReady;
wire ad9510ReadyToRecv = (state==R_AD9510) && (rcvRdReqCount<6);
wire readyToRecv =  (state==W_BE) || (state==W_EF) || (state==CHECK) || qBusReadyToRecv || ad9510ReadyToRecv;

assign fifoRdReq = (state==IDLE)? 0: !fifoEmpty && readyToRecv;

 
wire qBusDataValidW = (state==R_QBUS)&&(recvByteCount==2)&&fifoDataValid;
reg qBusDataValidR=0; always @(posedge clk) qBusDataValidR <= qBusDataValidW;
wire qBusDataValidRE =  qBusDataValidW && !qBusDataValidR;
assign qBusDataValid = qBusDataValidRE;

reg [23:0] dataR=0;

assign SCQvalid = (state==R_AD9510) && (rcvRdReqCount == 6);
//assign q = (state==AD9510)? dataR : {dataR[15:0], data};
assign q = (state==R_QBUS)?  {dataR[15:0], data} : 
                             {dataR[15:0], data};

assign updateParamsStart = (state==R_QBUS) && (fsmByteCount==4) && fifoDataValid;
assign updateParamsEnd = (state==R_QBUS) && (fsmByteCount>4)  && (bCountToRcv==0);
reg [7:0] fifoClrSignalDelay = 0;

always @(posedge clk) begin
   if(!nrst) begin
      state <= IDLE;
      //rcvCount <= 0;
      //SCQvalid <= 0;
      rcvRdReqCount <= 0;
      dataR <= 0;
      fsmByteCount <= 0;
   end
   else begin
      //fifioRdReqR <= (state==IDLE)? 0: !fifoEmpty;
      if(fifoRdReq) begin
         rcvRdReqCount <= rcvRdReqCount + 1;
      end
      
      fifoDataValid <= fifoRdReq;

       
      case(state)
      IDLE: begin
         state <= W_BE;
         fsmByteCount <= 0;
         rcvRdReqCount <= 0;
         stateReq <= 0;
         qBusReq <= 0;
         eBusReq <= 0;
         reqFifoClr <= 0;
         fifoClrSignalDelay <= 100;
         reqParamsSend <= 0;
      end
      W_BE: begin
         if(fifoDataValid) begin
            if(data == 8'hbe) begin
               state <= W_EF;
            end
            else begin
               rcvRdReqCount <= 0;
            end
         end
      end
      W_EF: begin
         if(fifoDataValid) begin
            if(data == 8'hef) begin
               state <= CHECK;
            end
            else begin
               state <= W_BE;
            end
         end
      end
      CHECK: begin
         recvByteCount <= 0;
         if(fifoDataValid) begin
            fsmByteCount <= 3;
            if(data == AD9510) begin
               state <= R_AD9510;
               //SCQvalid <= 0;
               //rcvCount <= 0;
               //dataR <= 0;
            end
            else if(data == QBUS) begin
               state <= R_QBUS;
            end
            else if(data == REQ_STATE) begin
               stateReq <= 1;
               state <= IDLE;
            end
            else if(data == REQ_QBUS) begin
               qBusReq <= 1;
               state <= IDLE;
            end
            else if(data == REQ_EBUS1) begin
               eBusReq[0] <= 1;
               state <= IDLE;
            end
            else if(data == REQ_EBUS2) begin
               eBusReq[1] <= 1;
               state <= IDLE;
            end
            else if(data == REQ_EBUS3) begin
               eBusReq[2] <= 1;
               state <= IDLE;
            end
            else if(data == REQ_EBUS4) begin
               eBusReq[3] <= 1;
               state <= IDLE;
            end
            else if(data == REQ_EBUS5) begin
               eBusReq[4] <= 1;
               state <= IDLE;
            end
            else if(data == REQ_EBUS6) begin
               eBusReq[5] <= 1;
               state <= IDLE;
            end
            else if(data == REQ_EBUS7) begin
               eBusReq[6] <= 1;
               state <= IDLE;
            end
            else if(data == REQ_EBUS8) begin
               eBusReq[7] <= 1;
               state <= IDLE;
            end
            else if(data == REQ_EBUS_ALL) begin
               eBusReq[7:0] <= 8'hff;
               state <= IDLE;
            end
            else if(data == REQ_FIFO_CLR) begin
               reqFifoClr <= 1;
               fifoClrSignalDelay <= 10;
               state <= DELAY;
            end
            else if(data == REQ_CARU_DIS) begin
               caru_en <= 0;
               state <= IDLE;
            end
            else if(data == REQ_CARU_ENA) begin            
               caru_en <= 1;
               state <= IDLE;
            end
            else if(data == REQ_PARAMS_SEND) begin
               reqParamsSend <= 1;
               state <= IDLE;
            end               
            else if(data == SEND_PARAMS_ON_TIM2_ON) begin
               sendParamsOnTim2 <= 1;
               state <= IDLE;
            end
            else if(data == SEND_PARAMS_ON_TIM2_OFF) begin
               sendParamsOnTim2 <= 0;
               state <= IDLE;
            end
            else if(data == EBUS_TEST_MDOE_ON) begin
               eBusTestMode <= 1;
            end
            else if(data == EBUS_TEST_MDOE_OFF) begin
               eBusTestMode <= 0;
            end
         end

      end
      
      DELAY: begin
         if(fifoClrSignalDelay == 0)  state <= IDLE;
         else fifoClrSignalDelay <= fifoClrSignalDelay - 1;
      end
      
      R_AD9510: begin
         if(rcvRdReqCount < 6) begin
            if(fifoDataValid) begin
               fsmByteCount <= fsmByteCount + 1;                  
               //if(rcvRdReqCount < 5) begin
                  dataR <= {dataR[15:0], data};
               //end
            end
            
         end
         if(rcvRdReqCount == 5) begin
            //state <= IDLE;
         end
            //SCQvalid <= (rcvRdReqCount>5) ;         
            //dataR <= ((rcvRdReqCount<=6)&&fifoDataValid)? {dataR[15:0], data[7:0]} : dataR;
            //dataR <= fifoDataValid && (fsmByteCount<6)? {dataR[15:0], data} : dataR;
            //if((fsmByteCount == 6) && fifoDataValid) begin
               //SCQvalid <= 1;
            //end
            if(SCQvalid&&SCQready) begin
               //SCQvalid <= 0;
               state <= IDLE;
               //fsmByteCount <= 0;
            end
      end
      
      R_QBUS: begin
         if(fifoDataValid) begin
            recvByteCount <= ((fsmByteCount>4)&&(recvByteCount<2))? recvByteCount+1 : 0 ;
            fsmByteCount <= fsmByteCount + 1;
            dataR <= {dataR[15:0], data};
         end
         if (fsmByteCount<5) begin
            if(fifoDataValid) begin
               bCountToRcv <= {bCountToRcv[7:0], data}/* - 16'd1*/;
            end      
         end
         else begin
            if(fifoDataValid) begin
               bCountToRcv <= bCountToRcv - 1;               
               //if(recvByteCount<2) begin
               //   dataR <= {dataR[15:0], data};
                  //recvByteCount <= recvByteCount+1;
               //end

            end
            if(bCountToRcv==0) begin
               state <= IDLE;
               //rcvRdReqCount <= 0;                  
               reqParamsSend <= 1;
            end
            
/*             if(qBusDataReady&&qBusDataValid) begin
               //recvByteCount <= 0;
               if(bCountToRcv==0) begin
                  state <= IDLE;
                  rcvRdReqCount <= 0;                  
               end
            end */
            
         end
         
         /* spiCtrlQ <= ((rcvRdReqCount<=6)&&(fifoDataValid))? {spiCtrlQ[15:0], data[7:0]} : spiCtrlQ;
         if((rcvRdReqCount == 6) && fifoDataValid) begin
            SCQvalid <= 1;
         end
         if(SCQvalid&&SCQready) begin
            SCQvalid <= 0;
            state <= IDLE;
            rcvRdReqCount <= 0;
         end */
      
      end     
      endcase
   end
   
end
   
                
endmodule
