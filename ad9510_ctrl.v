
module ad9510_ctrl(input nrst, 
				input clk,
            output AD9510_SCLK,
            output AD9510_SDIO,
            input AD9510_SDO,
            output reg AD9510_SS = 1,
            
            output reg dataReady=0,
            input dataRdAck,
            output reg [7:0] data,
            input [23:0] dataToSend,
            input valid,
            output reg ready=0,
            
            input rcvDataReadReady,
            output reg rcvDataValid=0,
            output reg [7:0] rcvData=0);

reg reset;

//reg [7:0] configArr [0:20];
reg [7:0] readAddrArr [0:3];

reg [23:0] configArr1[0:8];

localparam [3:0] /*START_DELAY = 4'd0,*/
                IDLE = 4'd1,
                SEND_DATA = 4'd2,
                SEND_DATA_DELAY = 4'd3,
                SEND_SYNC1 = 4'd4,
                SEND_SYNC_DELAY = 4'd5,
                SEND_SYNC2 = 4'd6,
                READ_DATA = 4'd7;
                
reg [3:0] state = IDLE; //START_DELAY /* synthesis noprune */; //0-idle,1-qbus,2-caru,3-ebus

wire spiEmpty;
reg spiEmptyR; always @(posedge clk) spiEmptyR <= spiEmpty;
wire spiEmptyRE = ({spiEmptyR,spiEmpty}==2'b01);
wire spiEmptyFE = ({spiEmptyR,spiEmpty}==2'b10);

reg [3:0] sendByteInd=0; //always @(posedge clk) sendByteInd <= sendByteInd + 1;
reg [4:0] sendBitInd=0;

//wire [7:0] byteToSend = configArr[sendByteInd];
wire [7:0] spiDataRecv;
wire wrReq = spiEmpty && (sendByteInd <15) && ((state==SEND_DATA) || (state==READ_DATA) || (state==SEND_SYNC1) ||  (state==SEND_SYNC2) );

reg [19:0] delay=20'hf;//20'hfffff;

/* SPI_Master ad9510_spiMaster(.nrst(nrst), .clk(clk), .sck(AD9510_SCLK), .miso(AD9510_SDO), .mosi(AD9510_SDIO), .ss(AD9510_SS),
                            .wrReq(wrReq), .empty(spiEmpty), .byteToSend(byteToSend), .dataRecvd(spiDataRecv)); */
reg [23:0] sendReg = 0;

reg [7:0] clockCnt = 0; 
parameter DIVISOR = 8'd36;
always @(posedge clk) begin
   clockCnt <= clockCnt + 1;
   if((clockCnt>=(DIVISOR-1)) || (AD9510_SS))
      clockCnt <= 8'd0;
end
wire clkLow = (clockCnt<DIVISOR/2)?1'b0:1'b1;
reg clkLowR = 0; always @(posedge clk)  clkLowR <= clkLow;
wire clkLowRise = clkLow&&!clkLowR;
wire clkLowFall = !clkLow&&clkLowR;

assign AD9510_SCLK = clkLow&!AD9510_SS;
assign AD9510_SDIO = sendReg[23];

always @(posedge clk) rcvDataValid <= rcvDataReadReady&&rcvDataValid ? 0: rcvDataValid; 

initial begin
/*    configArr[0] = 8'h80;
   configArr[1] = 8'h4c;
   configArr[2] = 8'h00;
   
   configArr[3] = 8'h00;
   configArr[4] = 8'h00; //8'h4e;
   configArr[5] = 8'h00;

   configArr[6] = 8'h00;
   configArr[7] = 8'h5a;
   configArr[8] = 8'h01;

   configArr[9] = 8'h00;
   configArr[10] = 8'h58;
   configArr[11] = 8'h04;

   configArr[12] = 8'h00;
   configArr[13] = 8'h58;
   configArr[14] = 8'h00; */
   
/*    readAddrArr[0] = 8'h80;
   readAddrArr[1] = 8'h4c;
   readAddrArr[2] = 8'h80;
   readAddrArr[3] = 8'h4e; */
   
   
/*    configArr1[0] = {16'h804c, 8'h00};
   configArr1[1] = {16'h004c, 8'h00};
   configArr1[2] = {16'h804c, 8'h00};
   configArr1[3] = {16'h004c, 8'h00};
   configArr1[4] = {16'h004e, 8'h00};
   configArr1[5] = {16'h005a, 8'h01};
   configArr1[6] = {16'h0058, 8'h04};
   configArr1[7] = {16'h0058, 8'h00}; */
end


always @(posedge clk) begin
   
   if(spiEmptyRE && (dataReady==0)) begin
      //dataReady <= 1;
      data <= spiDataRecv;
   end
   else if(dataRdAck) begin
      dataReady <= 0;
   end
   if(!nrst) begin
      state <= IDLE;
      ready <= 0;
   end
   else begin
      case (state)
      IDLE: begin
         if(delay > 0 ) begin
            delay <= delay - 1;
            ready <= 0;
            AD9510_SS <= 1;
         end
         else begin
            if(valid) begin
               sendBitInd <= 0;
               state <= SEND_DATA;
               AD9510_SS <= 0;
               sendReg <= dataToSend;
               ready <= 1;
               
            end
            else begin
               ready <= 1;
               AD9510_SS <= 1;
            end
         end
         
         /*if(delay > 0) begin
            delay <= delay-1;
         end
         else begin
            state <= SEND_DATA_DELAY;
            delay <= 20'hf;
            sendByteInd <= 0;   
            sendBitInd <= 0;            
         end*/
      end
      
      SEND_DATA: begin
         ready <= 0;
         if((sendBitInd > 23) && !clkLow) begin
            AD9510_SS <= 1;            
            delay <= 20'hff;
            state <= IDLE;
            //rcvDataValid <= 1;
         end
         else if (clkLowFall) begin
            sendBitInd <= sendBitInd + 1;   
            sendReg <= {sendReg[22:0], 1'h0};
         end
         rcvData  <= (sendBitInd>15)?{rcvData[6:0], AD9510_SDO}:0;
      end
     
      SEND_DATA_DELAY: begin
         if (delay>0) begin
            delay <= delay-1;
         end
         else begin
            if(sendByteInd<8) begin
               sendBitInd <= 0;
               sendByteInd <= sendByteInd + 1;
               state <= SEND_DATA;
               AD9510_SS <= 0;
               sendReg <= configArr1[sendByteInd];
            end
            else begin
               state <= IDLE; //START_DELAY;
               delay <= 20'hfffff;
               
            end
            //sendByteInd <= 0;
         end
      end
     
      SEND_SYNC1: begin
         if(sendByteInd < 12) begin
            if(spiEmpty) begin
               sendByteInd <= sendByteInd + 1;
            end
         end
         else begin
            state <= SEND_SYNC_DELAY;
            delay <= 20'hff;
         
         end
      end
      SEND_SYNC_DELAY: begin
         if (delay>0) begin
            delay <= delay-1;
         end
         else begin
            state <= SEND_SYNC2;
            //sendByteInd <= 0;
         end
      end
     
      SEND_SYNC2: begin
         if(sendByteInd < 15) begin
            if(spiEmpty) begin
               sendByteInd <= sendByteInd + 1;
            end
         end
         else begin
            state <= IDLE; //START_DELAY;
            delay <= 20'hfffff;
         
         end
      end

     
      READ_DATA: begin
         if(sendByteInd <6) begin
            if(spiEmpty) begin
               sendByteInd <= sendByteInd + 1;
            end
         end
      end

      IDLE: begin
      end
      endcase
   end
end

endmodule
