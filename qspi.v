module QSPI_Master(input nrst, 
				input clk, 
            input eBusReq,
				output sck, 
				input [3:0] miso, 
				output reg mosi = 0, 
				output reg ss=1,
				output reg [7:0] byteToSend,
            output reg [3:0] nLock=4'hf,
            output reg slavePresent=0,
            output reg [3:0] ebusFifoFull,
            output reg [15:0] eBusCh1Usedw, eBusCh2Usedw, eBusCh3Usedw, eBusCh4Usedw);

assign sck = clk;
reg [7:0] cycSpiMosi;
reg [3:0] cycSpiMiso;


parameter [1:0] START = 3'd0,
                SEND = 3'd1,
                STOP = 3'd2;
reg [1:0] spiSendState = START;
reg [9:0] spiStopDelay=0;


reg [7:0] spiSendByte = 8'h81;
//assign mosi = spiSendByte[7];
reg [31:0] spiRecvData = 0;

reg [3:0] spiEdgeCount = 8;

reg [31:0] recvdData=0;

reg [5:0] recvStateBit=1;

always @(posedge clk) begin
   if(!nrst) begin
      spiSendState <= START;
      ss <= 1;
      spiEdgeCount <= 8;
      recvStateBit[0] = 1;
      slavePresent <= 0;
   end
   else begin
      case(spiSendState) 
         START: begin
            ss <= 0;
            spiSendState <= SEND;
            spiEdgeCount <= 9;
            //cycSpiMosi <= spiSendByte;
				spiSendByte <= 8'h81; //byteToSend;
         end
         SEND: begin
            if (spiEdgeCount == 0) begin
               spiSendState <= STOP;
               spiStopDelay <= 10'h2;
               
               if(recvStateBit[0]) begin
                  recvStateBit <= (spiRecvData == 32'hdeadbea1) ? 2 : 1;                                    
                  slavePresent <= (spiRecvData == 32'hdeadbea1) ? 1 : 0;
                  //recvStateBit <= 6'h2;
                  //eBusCh1Usedw <= spiRecvData[15:0]; 
                  //eBusCh2Usedw <= spiRecvData[31:16]; 
               end
               else if(recvStateBit[1]) begin
                  recvStateBit <= (spiRecvData == 32'hdeadbea2) ? 4 : 1; 
                  slavePresent <= (spiRecvData == 32'hdeadbea2) ? 1 : 0;
                  //eBusCh3Usedw <= spiRecvData[15:0];
                  //eBusCh4Usedw <= spiRecvData[31:16];
               end
               else if(recvStateBit[2]) begin
                  recvStateBit <= 8;
                  nLock <= spiRecvData[3:0];
               end
               else if(recvStateBit[3]) begin
                  recvStateBit <= 16;
                  eBusCh1Usedw <=  spiRecvData[14:0]; //16'h1212;
                  eBusCh2Usedw <=  spiRecvData[30:16]; //16'h2434;       
                  ebusFifoFull[0] <= spiRecvData[15];
                  ebusFifoFull[1] <= spiRecvData[31];
               end
               else if(recvStateBit[4]) begin
                  recvStateBit <= 1;
                  eBusCh3Usedw <=  spiRecvData[14:0]; //16'h3656;
                  eBusCh4Usedw <=  spiRecvData[30:16]; //16'h4767;
                  ebusFifoFull[2] <= spiRecvData[15];
                  ebusFifoFull[3] <= spiRecvData[31];
               end
            end
            else begin
               if(spiEdgeCount == 1) ss <= 1;
               mosi <= spiSendByte[7];
               spiEdgeCount <= spiEdgeCount - 1;
               spiRecvData <= {spiRecvData[27:0], miso};
               spiSendByte <= {spiSendByte[6:0], 1'b0};					
            end

         end
         STOP: begin
            if (spiStopDelay>0) begin
               spiStopDelay <= spiStopDelay - 1;
            end
            else begin
               spiSendState <= START;
            end
         end
      endcase
      
      if(!ss) begin
         recvdData <= {recvdData[28:0], miso};
      end
   end
end

endmodule





module QSPI_Slave(input nrst, 
            input clk,
				input sck, 
				output reg [3:0] miso = 4'h0, 
				input mosi, 
				input ss, 
            output reg [7:0] recvdData,
            output reg dataRecvd,
            input [3:0] nLockBus,
            input [3:0] ebusFifoFull,
            input wire [15:0] eBusCh1Usedw,
            input wire [15:0] eBusCh2Usedw,
            input wire [15:0] eBusCh3Usedw,
            input wire [15:0] eBusCh4Usedw);
   
// sync SCK to the FPGA clock using a 3-bits shift register
//reg [2:0] SCKr;  always @(posedge clk) SCKr <= {SCKr[1:0], SCK};
//wire SCK_risingedge = (SCKr[2:1]==2'b01);  // now we can detect SCK rising edges
//wire SCK_fallingedge = (SCKr[2:1]==2'b10);  // and falling edges

// same thing for SSEL
reg [2:0] SSELr;  always @(posedge clk) SSELr <= {SSELr[1:0], ss};
wire SSEL_active = ~SSELr[1];  // SSEL is active low
wire SSEL_startmessage = (SSELr[0]==1'b1)&&(ss==1'b0);  // message starts at falling edge
wire SSEL_endmessage = (SSELr[0]==1'b0)&&(ss==1'b1);  // message stops at rising edge

reg [3:0] spiEdgeCount = 8;
reg [31:0] sendWord=0;
reg [7:0] sendState = 1;
always @(posedge sck) begin
   if(!nrst) begin
      //spiSendState <= START;
      //cycSpiSS <= 1;
      //spiEdgeCount <= 8;
      miso <= 4'h0;
      sendState <= 1;
   end
   else begin
      if(ss) begin         
         spiEdgeCount <= 0;
         if (sendState[0]) sendWord <= 32'hdeadbea1;
         if (sendState[1]) sendWord <= 32'hdeadbea2;
         if (sendState[2]) sendWord <= {28'h0, nLockBus};
         if (sendState[3]) sendWord <= {ebusFifoFull[1], eBusCh2Usedw[14:0], ebusFifoFull[0], eBusCh1Usedw[14:0]};//{16'h00ff, 16'h55aa};//
         if (sendState[4]) sendWord <= {ebusFifoFull[3], eBusCh4Usedw[14:0], ebusFifoFull[2], eBusCh3Usedw[14:0]}; //{16'h33cc, 16'hff00}; //
         if (sendState[5]) sendState <= 1;
      end
      else begin
         recvdData <= {recvdData[6:0], mosi};
         miso[3:0] <= sendWord[31:28];
         sendWord <= {sendWord[27:0], 4'h0};
         dataRecvd <= SSEL_endmessage;
         
         spiEdgeCount <= spiEdgeCount + 1;
         if(spiEdgeCount == 8) begin 
            sendState <= {sendState[6:0],1'h0};
            
         end
      end
   end
end
   
endmodule 