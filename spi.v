module SPI_Master(input nrst, 
				input clk, 
				output sck, 
				input  miso, 
				output mosi, 
				output reg ss=1,
				
				input wrReq,
				input [7:0] byteToSend,
				output reg empty=1,
				
				output reg [7:0] dataRecvd=0);

assign sck = clk;

/* parameter [1:0] START = 3'd0,
                SEND = 3'd1,
                STOP = 3'd2;
reg [1:0] spiSendState = START; */
                
reg [9:0] spiStopDelay=0;

reg [7:0] spiSendByte = 0;
assign mosi = spiSendByte[7];


reg [3:0] spiEdgeCount = 0;



always @(posedge clk) begin
   if(!nrst) begin
      spiEdgeCount <= 0;
      empty <= 1;
   end
   else begin
      if (spiEdgeCount == 0) begin
         if(wrReq) begin
            ss <= 0;
            spiSendByte <= byteToSend;
            empty <= 0;
            spiEdgeCount <= 7;
         end
         else begin
            ss <= 1;
            empty <= 1;
            spiStopDelay <= 10'h1;
         end
      end
      else begin
         spiEdgeCount <= spiEdgeCount - 1;
         spiSendByte <= {spiSendByte[6:0], 1'b0};
         dataRecvd <= {dataRecvd[6:0], miso};               					
      end
   end
end

endmodule
