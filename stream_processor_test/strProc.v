module streamProcessor(input wire reset, input wire clk,
				input wire [17:0] qBusData,
				input wire qBusEmpty,
				output qBusRdReqR,

				input wire [17:0] caruBusData,
				input wire caruBusEmpty,
				output caruBusRdReq,
				
				input wire [17:0] eBusData,
				input wire eBusEmpty,
				output eBusRdReq,
				
				input rdReq, 
            output empty,
				output [23:0] out);

reg [15:0] dataBuf; 
reg [1:0] outByteCount=0;

reg [1:0] state = 0; //0-idle,1-qbus,2-caru,3-ebus

reg [5:0] qBusEmptyDDD = 0;
reg [3:0] eBusEmptyDDD = 0;

reg emptyR;
reg [1:0] dataCnt=3;
reg [25:0] dataDelay=0;

wire [23:0] dataW = (dataCnt==3)?24'heeeeee:
					(dataCnt==2)?24'heeeeee:
               (dataCnt==1)?24'hee1122:
               (dataCnt==0)?24'hee3344:24'heeeeee;

wire emptyLoc = (dataDelay==0)? ((dataCnt==3)?0:
											(dataCnt==2)?0:
                                 (dataCnt==1)?0:
                                 (dataCnt==0)?1:0):1;
always @(posedge clk) begin
   qBusEmptyDDD <= {qBusEmptyDDD[4:0], !qBusEmpty};
   eBusEmptyDDD <= {eBusEmptyDDD[2:0], !eBusEmpty};
   
	case(state)
		0: begin
         if(dataDelay==0) begin
            if(dataCnt==0) begin
               dataCnt <= 3;
               dataDelay <= 26'h3FFFFFF;
            end
            else if(dataCnt==3)begin
               dataCnt <= dataCnt - 1;
            end
            else if(rdReq) begin
                  dataCnt <= dataCnt - 1;
            end
         end
         else begin
            dataDelay <= dataDelay-1;
            if(!qBusEmpty) begin
               state <= 1;
            end
            else if(!caruBusEmpty) begin
               state <= 2;
            end
            else if(!eBusEmpty) begin
               state <= 3;
            end
         end

		end
		1: begin  //qbus
			if(qBusEmptyDDD == 0) begin
				state <= 0;
			end
		end
		2: begin  //caru
		end
		3: begin  //ebus
      	if(eBusEmptyDDD == 0) begin
				state <= 0;
			end
		end
	endcase

end

assign empty=(state==2'd0)?emptyLoc:
               (state==2'd1)?qBusEmpty:
               (state==2'd2)?caruBusEmpty:
               (state==2'd3)?eBusEmpty:1;

assign qBusRdReqR   = (state==2'd1)?rdReq:0;
assign caruBusRdReq = (state==2'd2)?rdReq:0;
assign eBusRdReq    = (state==2'd3)?rdReq:0;
assign out = (state==2'd0)? dataW:
               (state==2'd1)?{4'ha, 2'h0, qBusData[17:0]}:
               (state==2'd2)?{4'hc, 2'h0, caruBusData[17:0]}:
               (state==2'd3)?{4'hd, 2'h0, eBusData[17:0]}:24'hfff;
               

//assign out = ;
//assign empty = ((outByteCount==0)||(outByteCount==1))&&inputRegFree;
//assign wrAllow = (outByteCount==1)||(outByteCount==0);
//assign q = (outByteCount==0)?data[7:0]:dataBuf[7:0];				

endmodule
