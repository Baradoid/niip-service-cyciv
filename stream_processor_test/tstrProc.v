`timescale 1 ns/ 1 ns
module test_streamProcessor;

reg reset, clk=0;
reg [17:0] qBusData;
reg qBusEmptyR;
wire qBusRdReq;

reg [17:0] eBusData;
reg eBusEmptyR;
wire eBusRdReq;

reg [17:0] caruBusData;
reg caruBusEmptyR;
wire caruBusRdReq;

wire [23:0] debugData;
wire [23:0] dataOut;

reg rdReqR;
wire wrAllow, empty;

//устанавливаем экземпл¤р тестируемого модул¤
streamProcessor streamProcessorInst(reset, clk, 
									qBusData, qBusEmptyR, qBusRdReq,
                           caruBusData, caruBusEmptyR, caruBusRdReq,
									eBusData, eBusEmptyR, eBusRdReq,
									rdReqR, empty, dataOut);

//моделируем сигнал тактовой частоты
always
  #10 clk = ~clk;

initial
begin
rdReqR <=0;
#40
   forever begin
      @(posedge clk) begin
         rdReqR<=~empty;
      end
   end   
   //@(posedge clk) 
   //rdReqR<=0;
end
 
initial
begin
   qBusData <= 24'h223344;
   qBusEmptyR <= 1;
  reset = 0;

//пауза длительностью "50"
   repeat(10)  begin
      #150
      qBusEmptyR <=0;
      repeat(4) begin
         @(posedge clk) 
         begin
            if(qBusRdReq) qBusData <= qBusData+1;
         end         
      end      
      qBusEmptyR <=1;
      #100;
   end
end

initial
begin
   eBusData <= 24'h556677;
   eBusEmptyR <= 1;   

//пауза длительностью "50"
  #250
  @(posedge clk) 
      eBusEmptyR <=0;
      repeat(10) begin
			@(posedge clk) 
			begin
            if(qBusRdReq) eBusData <= eBusData+1;
			end         
		end      
      eBusEmptyR <=1;
	#100;
end

//заканчиваем симул¤цию в момент времени "400"
initial
begin
  #4000 $finish;
end

//создаем файл VCD дл¤ последующего анализа сигналов
initial
begin
  $dumpfile("out.vcd");
  $dumpvars(0,test_streamProcessor);
end

//наблюдаем на некоторыми сигналами системы
initial
$monitor($stime,, reset,, clk,,, qBusData,, dataOut,, wrAllow);

endmodule