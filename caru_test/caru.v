module caru(input wire reset, 
				input wire clk, 
				input wire caruClkIn, caruDataIn,
				output caruClkOut, caruDataOut);

reg [1:0] state = 0; 

reg [2:0] symbCount = 0;
reg posEdgeState = 0; always @(posedge caruClkIn) posEdgeState <= caruDataIn;
wire [1:0] dataSymbol = {posEdgeState, caruDataIn};

reg allowTranslateClock = 0;

assign caruClkOut = allowTranslateClock? caruClkIn : 0;


//reg [1:0] caruDataSymbOutArrOut[0:6*8];
reg [7:0] caruDataOutArr[0:6];
reg [7:0] curDataByte;
reg [1:0] caruDataSymbOut=0;
reg [7:0] arrIdx = 0;
reg [3:0] bitIdx = 0;
assign caruDataOut=allowTranslateClock? (caruClkIn? caruDataSymbOut[1]: caruDataSymbOut[0]): 0;

initial begin
   caruDataOutArr[0] = 8'h00;
   caruDataOutArr[1] = 8'h00;
   caruDataOutArr[2] = 8'h00;
   caruDataOutArr[3] = 8'h00;
   caruDataOutArr[4] = 8'h00;
   caruDataOutArr[5] = 8'hff;

end

always @(negedge caruClkIn) begin
		
	if(state == 0) begin // wait for start
      allowTranslateClock <= (dataSymbol == 2'b11) && (symbCount>1);
      
		if(dataSymbol == 2'b00) begin   //start detected
			symbCount <= symbCount + 1;
		end
		else if((dataSymbol == 2'b11) && (symbCount>1)) begin //stop detected
			state <= 1;      			//step to send frame
			symbCount <= 4 ;			//send 4 start
			caruDataSymbOut <= 2'b00;
		end
		else begin
			symbCount <= 0;
		end
	end
	else if(state == 1) begin // send start
		
      curDataByte<= caruDataOutArr[0];         

		if(symbCount == 0) begin 
			state <=2;
         arrIdx <= 1;
         bitIdx <= 7;			
		end
      else begin
         symbCount <= symbCount - 1;
      end
		
	end
	else if(state == 2) begin // send frame
      caruDataSymbOut <= curDataByte[7]? 2'b01 : 2'b10;

      if(bitIdx == 0) begin
         bitIdx <= 7;
         arrIdx <= arrIdx + 1;
         curDataByte <= caruDataOutArr[arrIdx];         
      end
      else begin
         bitIdx <= bitIdx - 1;
         curDataByte <= {curDataByte[6:0], 1'b0};
      end
		
      if((arrIdx==6) && (bitIdx == 0)) begin 
			state <=3;   //send stop
			symbCount <= 0;         
         
		end

	end
   else if(state == 3) begin  // send stop
      caruDataSymbOut <= 2'b11;
      state <=0;   // wait for next start
   end

end

endmodule
