`timescale 1 ns/ 1 ns
module test_caru;

reg reset, clk;
reg [23:0]wdata;
wire [23:0] debugData;
wire [7:0] dataOut;

reg wrReq;
wire rdReq = (wrAllow&(~wrReq));
wire wrAllow, dataValid;

reg caruClkIn=0, caruDataIn=0;
wire caruClkOut, caruDataOut;

integer caruClkCount = 0;
//устанавливаем экземпл¤р тестируемого модул¤
caru caru_inst(reset, clk, caruClkIn, caruDataIn, caruClkOut, caruDataOut);

//моделируем сигнал тактовой частоты
always
  #10 clk = ~clk;

//от начала времени...

initial
begin
  clk = 0;
  reset = 1;
  wdata = 24'h223344;
  wrReq = 1'b0;
  //rdReq = 1'b0;

	repeat(1) begin
		@(posedge clk);
	end
  reset = 0;

//пауза длительностью "50"
  #10
	forever begin
		caruClkCount <= 0;
		repeat(80) begin
			@(posedge clk) begin
				//#1
				//rdReq <= (wrAllow&(~rdReq));
				wrReq <= rdReq;
				if(rdReq==1) wdata <= wdata+1;    
				
				caruClkIn <= 1;
			end

			@(negedge clk)  begin
				caruClkIn <= 0;
			end
			caruClkCount <= caruClkCount + 1;
			#1;
			if(caruClkCount == 5) caruDataIn <= 1;
		end
		caruDataIn <= 0;
	#100;
	
	end
	
end

//заканчиваем симул¤цию в момент времени "400"
initial
begin
  #10000 $finish;
end

//создаем файл VCD дл¤ последующего анализа сигналов
initial
begin
  $dumpfile("out.vcd");
  $dumpvars(0,test_caru);
end

//наблюдаем на некоторыми сигналами системы
initial
$monitor($stime,, reset,, clk,,, wdata,, wrReq,, dataOut,, wrAllow);

endmodule