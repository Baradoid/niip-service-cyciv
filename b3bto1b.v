module b3bto1b(input wire nrst, 
				input wire clk, 
				input wire [23:0] data, 
				input wrReq, 
				
				output ready,
				input outReady,
				output dataValid,
				output [7:0] q);

reg [23:0] dataBuf; 
reg [1:0] outByteCount=0;
//assign empty = ((outByteCount==0)||(outByteCount==1))&&inputRegFree;
assign ready = (outByteCount==1)||((outByteCount==0)&&!wrReq);
assign q = dataBuf[23:16];				
assign dataValid = (outByteCount>0)&&outReady;


always @(posedge clk) begin
   if(!nrst) begin
      outByteCount <= 0;
   end
   else begin
      if(wrReq) begin
         dataBuf <= data;		
         outByteCount <= 3;
      end
      else if( (outByteCount>0)&& (outReady))   begin
         dataBuf <= {dataBuf[15:0], 8'h0};
         outByteCount <= outByteCount-1;		
      end
   end

end


endmodule


/* module b3bto1b(input wire nrst, 
				input wire clk, 
				input wire [23:0] data, 
				input wrReq, 
				
				output ready,
				input outReady,
				output dataValid,
				output [7:0] q);

reg [15:0] dataBuf; 
reg [1:0] outByteCount=0;
//assign empty = ((outByteCount==0)||(outByteCount==1))&&inputRegFree;
assign ready = (outByteCount==1)||((outByteCount==0)&&!wrReq);
assign q = wrReq ? data[23:16]:dataBuf[15:8];				
assign dataValid = ((outByteCount>0)||wrReq)&&outReady;


always @(posedge clk) begin
   if(!nrst) begin
      outByteCount <= 0;
   end
   else begin
      if(wrReq) begin
         dataBuf <= data[15:0];		
         outByteCount <= 2;
      end
      else if( (outByteCount>0)&& (outReady))   begin
         dataBuf <= {dataBuf[7:0], 8'h0};
         outByteCount <= outByteCount-1;		
      end
   end

end


endmodule
 */