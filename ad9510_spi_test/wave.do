onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /spi_test_top/reset
add wave -noupdate -expand -group M /spi_test_top/topInstM/TIM1
add wave -noupdate -expand -group M /spi_test_top/topInstM/TIM2
add wave -noupdate -expand -group M /spi_test_top/topInstM/GPIO1
add wave -noupdate -expand -group M -group usbFifoRx -radix hexadecimal /spi_test_top/topInstM/usbRxFifo88/data
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/wrclk
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/wrreq
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/rdclk
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/rdreq
add wave -noupdate -expand -group M -group usbFifoRx -radix hexadecimal /spi_test_top/topInstM/usbRxFifo88/q
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/rdempty
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/rdusedw
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/wrfull
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/sub_wire0
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/sub_wire1
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/sub_wire2
add wave -noupdate -expand -group M -group usbFifoRx /spi_test_top/topInstM/usbRxFifo88/sub_wire3
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/nrst
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/clk
add wave -noupdate -expand -group M -group protoParser -radix hexadecimal /spi_test_top/topInstM/UsbProtoParserInst/state
add wave -noupdate -expand -group M -group protoParser -radix hexadecimal /spi_test_top/topInstM/UsbProtoParserInst/data
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/fifoEmpty
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/fifoRdReq
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/fifoUsedw
add wave -noupdate -expand -group M -group protoParser -radix hexadecimal /spi_test_top/topInstM/UsbProtoParserInst/dataR
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/qBusReady
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/qBusReadyToRecv
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/sendParamsOnTim2
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/readyToRecv
add wave -noupdate -expand -group M -group protoParser -expand -group params /spi_test_top/topInstM/paramsFSMState
add wave -noupdate -expand -group M -group protoParser -expand -group params -radix hexadecimal /spi_test_top/topInstM/paramsMemAddr
add wave -noupdate -expand -group M -group protoParser -expand -group params -radix hexadecimal /spi_test_top/topInstM/paramsMemDataLen
add wave -noupdate -expand -group M -group protoParser -expand -group params /spi_test_top/topInstM/UsbProtoParserInst/qBusDataValid
add wave -noupdate -expand -group M -group protoParser -radix hexadecimal /spi_test_top/topInstM/UsbProtoParserInst/q
add wave -noupdate -expand -group M -group protoParser -group {New Group} /spi_test_top/topInstM/UsbProtoParserInst/SCQready
add wave -noupdate -expand -group M -group protoParser -group {New Group} /spi_test_top/topInstM/UsbProtoParserInst/SCQvalid
add wave -noupdate -expand -group M -group protoParser -group {New Group} /spi_test_top/topInstM/UsbProtoParserInst/caru_en
add wave -noupdate -expand -group M -group protoParser -group {New Group} /spi_test_top/topInstM/UsbProtoParserInst/reqFifoClr
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/stateReq
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/qBusReq
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/eBusReq
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/stateW
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/rcvRdReqCount
add wave -noupdate -expand -group M -group protoParser -radix hexadecimal /spi_test_top/topInstM/UsbProtoParserInst/fsmByteCount
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/fifoDataValid
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/fifioRdReqR
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/bCountToRcv
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/recvByteCount
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/ad9510ReadyToRecv
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/qBusDataValidW
add wave -noupdate -expand -group M -group protoParser /spi_test_top/topInstM/UsbProtoParserInst/qBusDataValidR
add wave -noupdate -expand -group M /spi_test_top/topInstM/UsbProtoParserInst/qBusDataValidRE
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/aclr
add wave -noupdate -expand -group M -group fifo1818_UprTx -radix hexadecimal /spi_test_top/topInstM/fifo1818_QbusTx/data
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/rdclk
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/rdreq
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/wrclk
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/wrreq
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/q
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/rdempty
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/rdusedw
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/wrfull
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/sub_wire0
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/sub_wire1
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/sub_wire2
add wave -noupdate -expand -group M -group fifo1818_UprTx /spi_test_top/topInstM/fifo1818_QbusTx/sub_wire3
add wave -noupdate -expand -group M -group eBusFifo1 /spi_test_top/topInstM/ch1Fifo/aclr
add wave -noupdate -expand -group M -group eBusFifo1 /spi_test_top/topInstM/ch1Fifo/wrclk
add wave -noupdate -expand -group M -group eBusFifo1 /spi_test_top/topInstM/ch1Fifo/wrreq
add wave -noupdate -expand -group M -group eBusFifo1 /spi_test_top/topInstM/ch1Fifo/wrfull
add wave -noupdate -expand -group M -group eBusFifo1 -radix hexadecimal -childformat {{{/spi_test_top/topInstM/ch1Fifo/data[17]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[16]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[15]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[14]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[13]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[12]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[11]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[10]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[9]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[8]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[7]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[6]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[5]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[4]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[3]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[2]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[1]} -radix hexadecimal} {{/spi_test_top/topInstM/ch1Fifo/data[0]} -radix hexadecimal}} -subitemconfig {{/spi_test_top/topInstM/ch1Fifo/data[17]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[16]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[15]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[14]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[13]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[12]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[11]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[10]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[9]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[8]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[7]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[6]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[5]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[4]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[3]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[2]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[1]} {-height 15 -radix hexadecimal} {/spi_test_top/topInstM/ch1Fifo/data[0]} {-height 15 -radix hexadecimal}} /spi_test_top/topInstM/ch1Fifo/data
add wave -noupdate -expand -group M -group eBusFifo1 /spi_test_top/topInstM/ch1Fifo/rdclk
add wave -noupdate -expand -group M -group eBusFifo1 /spi_test_top/topInstM/ch1Fifo/rdreq
add wave -noupdate -expand -group M -group eBusFifo1 -radix hexadecimal /spi_test_top/topInstM/ch1Fifo/q
add wave -noupdate -expand -group M -group eBusFifo1 /spi_test_top/topInstM/ch1Fifo/rdempty
add wave -noupdate -expand -group M -group eBusFifo1 /spi_test_top/topInstM/ch1Fifo/rdusedw
add wave -noupdate -expand -group M -group eBusFifo1 -group {New Group} /spi_test_top/topInstM/ch1Fifo/sub_wire0
add wave -noupdate -expand -group M -group eBusFifo1 -group {New Group} /spi_test_top/topInstM/ch1Fifo/sub_wire1
add wave -noupdate -expand -group M -group eBusFifo1 -group {New Group} /spi_test_top/topInstM/ch1Fifo/sub_wire2
add wave -noupdate -expand -group M -group eBusFifo1 -group {New Group} /spi_test_top/topInstM/ch1Fifo/sub_wire3
add wave -noupdate -expand -group M -group eBusFifo2 /spi_test_top/topInstM/ch2Fifo/aclr
add wave -noupdate -expand -group M -group eBusFifo2 -radix hexadecimal /spi_test_top/topInstM/ch2Fifo/data
add wave -noupdate -expand -group M -group eBusFifo2 /spi_test_top/topInstM/ch2Fifo/rdclk
add wave -noupdate -expand -group M -group eBusFifo2 /spi_test_top/topInstM/ch2Fifo/rdreq
add wave -noupdate -expand -group M -group eBusFifo2 /spi_test_top/topInstM/ch2Fifo/wrclk
add wave -noupdate -expand -group M -group eBusFifo2 /spi_test_top/topInstM/ch2Fifo/wrreq
add wave -noupdate -expand -group M -group eBusFifo2 -radix hexadecimal /spi_test_top/topInstM/ch2Fifo/q
add wave -noupdate -expand -group M -group eBusFifo2 /spi_test_top/topInstM/ch2Fifo/rdempty
add wave -noupdate -expand -group M -group eBusFifo2 /spi_test_top/topInstM/ch2Fifo/rdusedw
add wave -noupdate -expand -group M -group eBusFifo2 /spi_test_top/topInstM/ch2Fifo/wrfull
add wave -noupdate -expand -group M -group eBusFifo2 -group {New Group} /spi_test_top/topInstM/ch2Fifo/sub_wire0
add wave -noupdate -expand -group M -group eBusFifo2 -group {New Group} /spi_test_top/topInstM/ch2Fifo/sub_wire1
add wave -noupdate -expand -group M -group eBusFifo2 -group {New Group} /spi_test_top/topInstM/ch2Fifo/sub_wire2
add wave -noupdate -expand -group M -group eBusFifo2 -group {New Group} /spi_test_top/topInstM/ch2Fifo/sub_wire3
add wave -noupdate -expand -group M -group eBusFifo3 /spi_test_top/topInstM/ch3Fifo/aclr
add wave -noupdate -expand -group M -group eBusFifo3 -radix hexadecimal /spi_test_top/topInstM/ch3Fifo/data
add wave -noupdate -expand -group M -group eBusFifo3 /spi_test_top/topInstM/ch3Fifo/rdclk
add wave -noupdate -expand -group M -group eBusFifo3 /spi_test_top/topInstM/ch3Fifo/rdreq
add wave -noupdate -expand -group M -group eBusFifo3 /spi_test_top/topInstM/ch3Fifo/wrclk
add wave -noupdate -expand -group M -group eBusFifo3 /spi_test_top/topInstM/ch3Fifo/wrreq
add wave -noupdate -expand -group M -group eBusFifo3 /spi_test_top/topInstM/ch3Fifo/q
add wave -noupdate -expand -group M -group eBusFifo3 /spi_test_top/topInstM/ch3Fifo/rdempty
add wave -noupdate -expand -group M -group eBusFifo3 /spi_test_top/topInstM/ch3Fifo/rdusedw
add wave -noupdate -expand -group M -group eBusFifo3 /spi_test_top/topInstM/ch3Fifo/wrfull
add wave -noupdate -expand -group M -group eBusFifo3 -expand -group {New Group} /spi_test_top/topInstM/ch3Fifo/sub_wire0
add wave -noupdate -expand -group M -group eBusFifo3 -expand -group {New Group} /spi_test_top/topInstM/ch3Fifo/sub_wire1
add wave -noupdate -expand -group M -group eBusFifo3 -expand -group {New Group} /spi_test_top/topInstM/ch3Fifo/sub_wire2
add wave -noupdate -expand -group M -group eBusFifo3 -expand -group {New Group} /spi_test_top/topInstM/ch3Fifo/sub_wire3
add wave -noupdate -expand -group M /spi_test_top/topInstM/eBusReq
add wave -noupdate -expand -group M {/spi_test_top/topInstM/eBusReq[0]}
add wave -noupdate -expand -group M -expand -group sdram1_qsys_input /spi_test_top/topInstM/sdram1WaitReq
add wave -noupdate -expand -group M -expand -group sdram1_qsys_input -radix hexadecimal /spi_test_top/topInstM/sdram1WrData
add wave -noupdate -expand -group M -expand -group sdram1_qsys_input /spi_test_top/topInstM/sdram1WrReqW
add wave -noupdate -expand -group M -expand -group sdram1_qsys_input -radix hexadecimal /spi_test_top/topInstM/sdram1Addr
add wave -noupdate -expand -group M -expand -group sdram1_qsys_input /spi_test_top/topInstM/sdram_inst/sdram_controller_0_s1_byteenable_n
add wave -noupdate -expand -group M -expand -group sdram1_qsys_input /spi_test_top/topInstM/sdram_inst/sdram_controller_0_s1_address
add wave -noupdate -expand -group M -expand -group sdram1_qsys_input /spi_test_top/topInstM/sdram_inst/sdram_controller_0_wire_ba
add wave -noupdate -expand -group M -expand -group sdram1_qsys_input /spi_test_top/topInstM/SDRAM1_BA
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/nRst
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/clk
add wave -noupdate -expand -group M -group sdramCtrlInst1 -radix hexadecimal /spi_test_top/topInstM/sdramCtrlInst1/sdramState
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/ebus1FifoQ
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/ebus2FifoQ
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/ebusFifoEmpty
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/ebusFifoRdReq
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/eBusReq
add wave -noupdate -expand -group M -group sdramCtrlInst1 -expand /spi_test_top/topInstM/sdramCtrlInst1/ebusReqR
add wave -noupdate -expand -group M -group sdramCtrlInst1 -radix unsigned /spi_test_top/topInstM/sdramCtrlInst1/wordsToRead
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/reqFifoClr
add wave -noupdate -expand -group M -group sdramCtrlInst1 -group sdramInFifo -radix hexadecimal /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoData
add wave -noupdate -expand -group M -group sdramCtrlInst1 -group sdramInFifo /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoWrReq
add wave -noupdate -expand -group M -group sdramCtrlInst1 -group sdramInFifo /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoAF
add wave -noupdate -expand -group M -group sdramCtrlInst1 -group sdramInFifo /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoF
add wave -noupdate -expand -group M -group sdramCtrlInst1 -group sdramInFifo /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoRdReq
add wave -noupdate -expand -group M -group sdramCtrlInst1 -group sdramInFifo /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoState
add wave -noupdate -expand -group M -group sdramCtrlInst1 -group sdramInFifo -radix hexadecimal /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoQ
add wave -noupdate -expand -group M -group sdramCtrlInst1 -group sdramInFifo /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoE
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/pref1
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/pref2
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramWrData
add wave -noupdate -expand -group M -group sdramCtrlInst1 -radix unsigned /spi_test_top/topInstM/sdramCtrlInst1/sdramAddr
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramRdReqW
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramWrReqW
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramRdData
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramRdValid
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramWaitReq
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramOutFifoQ
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramFifoRdReq
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramOutFifoE
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramFull
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/delay
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoERR
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramInFifoRdReqR
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdram2RdData
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdram2RdValid
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/wr_data
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramOutFifoAF
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramOutFifoWrusedw
add wave -noupdate -expand -group M -group sdramCtrlInst1 /spi_test_top/topInstM/sdramCtrlInst1/sdramFifoF
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/nRst
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/clk
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/ebus1FifoQ
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/ebus2FifoQ
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/ebusFifoEmpty
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/ebusFifoRdReq
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/eBusReq
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/reqFifoClr
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramWrData
add wave -noupdate -expand -group M -group sdramCtrlInst2 -radix hexadecimal /spi_test_top/topInstM/sdramCtrlInst2/sdramAddr
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramRdReqW
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramWrReqW
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramRdData
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramRdValid
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramWaitReq
add wave -noupdate -expand -group M -group sdramCtrlInst2 -group fifoOut -radix hexadecimal /spi_test_top/topInstM/SDRAM2_D
add wave -noupdate -expand -group M -group sdramCtrlInst2 -group fifoOut /spi_test_top/topInstM/sdramCtrlInst2/sdramFifoRdReq
add wave -noupdate -expand -group M -group sdramCtrlInst2 -group fifoOut /spi_test_top/topInstM/sdramCtrlInst2/sdramOutFifoE
add wave -noupdate -expand -group M -group sdramCtrlInst2 -group fifoOut -radix hexadecimal /spi_test_top/topInstM/sdramCtrlInst2/sdramOutFifoQ
add wave -noupdate -expand -group M -group sdramCtrlInst2 -group fifoOut /spi_test_top/topInstM/sdramCtrlInst2/sdramOutFifoAF
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramFull
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoData
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoWrReq
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoAF
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoF
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoState
add wave -noupdate -expand -group M -group sdramCtrlInst2 -radix hexadecimal /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoQ
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoE
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoRdReq
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/delay
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramState
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoERR
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramInFifoRdReqR
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdram2RdData
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdram2RdValid
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/wr_data
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/wordsToRead
add wave -noupdate -expand -group M -group sdramCtrlInst2 /spi_test_top/topInstM/sdramCtrlInst2/sdramFifoF
add wave -noupdate -expand -group M -expand -group sdram1 -radix hexadecimal /spi_test_top/m_sdram1/Dq
add wave -noupdate -expand -group M -expand -group sdram1 -radix hexadecimal /spi_test_top/m_sdram1/Addr
add wave -noupdate -expand -group M -expand -group sdram1 -expand /spi_test_top/m_sdram1/Ba
add wave -noupdate -expand -group M -expand -group sdram1 /spi_test_top/m_sdram1/Clk
add wave -noupdate -expand -group M -expand -group sdram1 /spi_test_top/m_sdram1/Cke
add wave -noupdate -expand -group M -expand -group sdram1 /spi_test_top/m_sdram1/Cs_n
add wave -noupdate -expand -group M -expand -group sdram1 /spi_test_top/m_sdram1/Ras_n
add wave -noupdate -expand -group M -expand -group sdram1 /spi_test_top/m_sdram1/Cas_n
add wave -noupdate -expand -group M -expand -group sdram1 /spi_test_top/m_sdram1/We_n
add wave -noupdate -expand -group M -expand -group sdram1 /spi_test_top/m_sdram1/Dqm
add wave -noupdate -expand -group M -group sdram2 /spi_test_top/m_sdram2/Dq
add wave -noupdate -expand -group M -group sdram2 -radix hexadecimal /spi_test_top/m_sdram2/Addr
add wave -noupdate -expand -group M -group sdram2 /spi_test_top/m_sdram2/Ba
add wave -noupdate -expand -group M -group sdram2 /spi_test_top/m_sdram2/Clk
add wave -noupdate -expand -group M -group sdram2 /spi_test_top/m_sdram2/Cke
add wave -noupdate -expand -group M -group sdram2 /spi_test_top/m_sdram2/Cs_n
add wave -noupdate -expand -group M -group sdram2 /spi_test_top/m_sdram2/Ras_n
add wave -noupdate -expand -group M -group sdram2 /spi_test_top/m_sdram2/Cas_n
add wave -noupdate -expand -group M -group sdram2 /spi_test_top/m_sdram2/We_n
add wave -noupdate -expand -group M -group sdram2 /spi_test_top/m_sdram2/Dqm
add wave -noupdate -expand -group M -group SDRAM_FSM /spi_test_top/topInstM/clkInt
add wave -noupdate -expand -group M -group SDRAM_FSM /spi_test_top/topInstM/sdramCtrlInst1/sdramState
add wave -noupdate -expand -group M -group SDRAM_FSM /spi_test_top/topInstM/sdramCtrlInst1/eBusReq
add wave -noupdate -expand -group M -group SDRAM_FSM /spi_test_top/topInstM/sdram1RdValid
add wave -noupdate -expand -group M -group SDRAM_FSM /spi_test_top/topInstM/sdram1WaitReq
add wave -noupdate -expand -group M -group SDRAM_FSM {/spi_test_top/topInstM/ebusFifoRdReq[0]}
add wave -noupdate -expand -group M -group SDRAM_FSM -radix hexadecimal /spi_test_top/topInstM/sdram1Addr
add wave -noupdate -expand -group M -group SDRAM_FSM -radix hexadecimal /spi_test_top/topInstM/sdram1WrData
add wave -noupdate -expand -group M -group SDRAM_FSM -radix hexadecimal /spi_test_top/topInstM/sdram1RdData
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/nrst
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/clk
add wave -noupdate -expand -group M -group streamProc -radix unsigned /spi_test_top/topInstM/streamProcessorInst/dataLen
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/ready
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/dataValid
add wave -noupdate -expand -group M -group streamProc -radix hexadecimal /spi_test_top/topInstM/streamProcessorInst/q
add wave -noupdate -expand -group M -group streamProc -radix hexadecimal /spi_test_top/topInstM/streamProcessorInst/state
add wave -noupdate -expand -group M -group streamProc -radix hexadecimal /spi_test_top/topInstM/streamProcessorInst/dataSendState
add wave -noupdate -expand -group M -group streamProc -radix unsigned -childformat {{{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[29]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[28]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[27]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[26]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[25]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[24]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[23]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[22]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[21]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[20]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[19]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[18]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[17]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[16]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[15]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[14]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[13]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[12]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[11]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[10]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[9]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[8]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[7]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[6]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[5]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[4]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[3]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[2]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[1]} -radix unsigned} {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[0]} -radix unsigned}} -subitemconfig {{/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[29]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[28]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[27]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[26]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[25]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[24]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[23]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[22]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[21]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[20]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[19]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[18]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[17]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[16]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[15]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[14]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[13]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[12]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[11]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[10]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[9]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[8]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[7]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[6]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[5]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[4]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[3]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[2]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[1]} {-height 15 -radix unsigned} {/spi_test_top/topInstM/streamProcessorInst/sdram1DataCount[0]} {-height 15 -radix unsigned}} /spi_test_top/topInstM/streamProcessorInst/sdram1DataCount
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/stateReq
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/qBusReq
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusReq
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/qBusData
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/qBusEmpty
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/qBusUsedw
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/qBusRdReq
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/qBus_nLock
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/qBus_wrFull
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/caruBusData
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/caruBusEmpty
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/caruBusPresent
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/caruBusRdReq
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/carBusWrFull
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/sdram1OutFifoEmpty
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/sdram1OutFifoRdReq
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/sdram1OutFifoDataValid
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBus_nLock
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusEmpty
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusWrFull
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBus1Data
add wave -noupdate -expand -group M -group streamProc -group {New Group} -radix hexadecimal /spi_test_top/topInstM/streamProcessorInst/eBusCh1Usedw
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBus2Data
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusCh2Usedw
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBus3Data
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusCh3Usedw
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBus4Data
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusCh4Usedw
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusCh5Usedw
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusCh6Usedw
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusCh7Usedw
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/eBusCh8Usedw
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/ad9510Data
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/ad9510DataReady
add wave -noupdate -expand -group M -group streamProc -group {New Group} /spi_test_top/topInstM/streamProcessorInst/ad9510DataRdAck
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/slaveFpgaPresent
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/rdReq
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/empty
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/out
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/sop
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/dataBuf
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/outByteCount
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/qBusEmptyDDD
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/qBusUsedwR
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/qBusUsedwNotChangedCnt
add wave -noupdate -expand -group M -group streamProc -group b3b1 /spi_test_top/topInstM/streamProcessorInst/b3b1WrReq
add wave -noupdate -expand -group M -group streamProc -group b3b1 -radix hexadecimal /spi_test_top/topInstM/streamProcessorInst/b3b1Data
add wave -noupdate -expand -group M -group streamProc -group b3b1 /spi_test_top/topInstM/streamProcessorInst/b3bto1bReady
add wave -noupdate -expand -group M -group streamProc -group b3b1 /spi_test_top/topInstM/streamProcessorInst/b3to1DataValid
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/rdReqW
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/qBusDataValid
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/ebusInd
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/eBus1EmptyDDD
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/eBus2EmptyDDD
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/emptyR
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/usb3bto1bWrReq
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/usb3to1DataOut
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/dataR
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/stateReqR
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/qbusReqR
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/i
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/ebusReqR
add wave -noupdate -expand -group M -group streamProc /spi_test_top/topInstM/streamProcessorInst/fifoWrClk
add wave -noupdate -expand -group M -group usbtTxFifo /spi_test_top/topInstM/usbTxFifo88/wrclk
add wave -noupdate -expand -group M -group usbtTxFifo /spi_test_top/topInstM/usbTxFifo88/wrreq
add wave -noupdate -expand -group M -group usbtTxFifo -radix hexadecimal /spi_test_top/topInstM/usbTxFifo88/data
add wave -noupdate -expand -group M -group usbtTxFifo /spi_test_top/topInstM/usbTxFifo88/wrfull
add wave -noupdate -expand -group M -group usbtTxFifo /spi_test_top/topInstM/usbTxFifo88/rdclk
add wave -noupdate -expand -group M -group usbtTxFifo /spi_test_top/topInstM/usbTxFifo88/rdreq
add wave -noupdate -expand -group M -group usbtTxFifo -radix hexadecimal /spi_test_top/topInstM/usbTxFifo88/q
add wave -noupdate -expand -group M -group usbtTxFifo /spi_test_top/topInstM/usbTxFifo88/rdempty
add wave -noupdate -expand -group M -group usbtTxFifo -radix unsigned /spi_test_top/topInstM/usbTxFifo88/rdusedw
add wave -noupdate -expand -group M -group usbtTxFifo -group {New Group} /spi_test_top/topInstM/usbTxFifo88/sub_wire0
add wave -noupdate -expand -group M -group usbtTxFifo -group {New Group} /spi_test_top/topInstM/usbTxFifo88/sub_wire1
add wave -noupdate -expand -group M -group usbtTxFifo -group {New Group} /spi_test_top/topInstM/usbTxFifo88/sub_wire2
add wave -noupdate -expand -group M -group usbtTxFifo -group {New Group} /spi_test_top/topInstM/usbTxFifo88/sub_wire3
add wave -noupdate -expand -group M -group USB /spi_test_top/topInstM/USB_FT_CLKIN
add wave -noupdate -expand -group M -group USB /spi_test_top/topInstM/USB_FT_nRXF
add wave -noupdate -expand -group M -group USB /spi_test_top/topInstM/USB_FT_nOE
add wave -noupdate -expand -group M -group USB /spi_test_top/topInstM/USB_FT_nRD
add wave -noupdate -expand -group M -group USB /spi_test_top/topInstM/USB_FT_nWR
add wave -noupdate -expand -group M -group USB -radix hexadecimal /spi_test_top/topInstM/USB_FT_D
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/nrst
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/clk
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/sck
add wave -noupdate -expand -group M -group spiMaster -radix hexadecimal /spi_test_top/topInstM/spiMaster/miso
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/mosi
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/ss
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/byteToSend
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/nLock
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/slavePresent
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/ebusFifoFull
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/eBusCh1Usedw
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/eBusCh2Usedw
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/eBusCh3Usedw
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/eBusCh4Usedw
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/cycSpiMosi
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/cycSpiMiso
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/spiSendState
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/spiStopDelay
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/spiSendByte
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/spiRecvData
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/spiEdgeCount
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/recvdData
add wave -noupdate -expand -group M -group spiMaster /spi_test_top/topInstM/spiMaster/recvStateBit
add wave -noupdate -expand -group S /spi_test_top/topInstS/reset
add wave -noupdate -expand -group S -group eBusFifo1 /spi_test_top/topInstS/ch1Fifo/aclr
add wave -noupdate -expand -group S -group eBusFifo1 -radix hexadecimal /spi_test_top/topInstS/ch1Fifo/data
add wave -noupdate -expand -group S -group eBusFifo1 /spi_test_top/topInstS/ch1Fifo/rdclk
add wave -noupdate -expand -group S -group eBusFifo1 /spi_test_top/topInstS/ch1Fifo/rdreq
add wave -noupdate -expand -group S -group eBusFifo1 /spi_test_top/topInstS/ch1Fifo/wrclk
add wave -noupdate -expand -group S -group eBusFifo1 /spi_test_top/topInstS/ch1Fifo/wrreq
add wave -noupdate -expand -group S -group eBusFifo1 -radix hexadecimal /spi_test_top/topInstS/ch1Fifo/q
add wave -noupdate -expand -group S -group eBusFifo1 /spi_test_top/topInstS/ch1Fifo/rdempty
add wave -noupdate -expand -group S -group eBusFifo1 -radix hexadecimal /spi_test_top/topInstS/ch1Fifo/rdusedw
add wave -noupdate -expand -group S -group eBusFifo1 /spi_test_top/topInstS/ch1Fifo/wrfull
add wave -noupdate -expand -group S -group eBusFifo1 -radix hexadecimal /spi_test_top/topInstS/ch1Fifo/sub_wire0
add wave -noupdate -expand -group S -group eBusFifo1 /spi_test_top/topInstS/ch1Fifo/sub_wire1
add wave -noupdate -expand -group S -group eBusFifo1 -radix hexadecimal /spi_test_top/topInstS/ch1Fifo/sub_wire2
add wave -noupdate -expand -group S -group eBusFifo1 /spi_test_top/topInstS/ch1Fifo/sub_wire3
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/aclr
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/data
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/rdclk
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/rdreq
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/wrclk
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/wrreq
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/q
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/rdempty
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/rdusedw
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/wrfull
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/sub_wire0
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/sub_wire1
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/sub_wire2
add wave -noupdate -expand -group S -group eBusFifo2 /spi_test_top/topInstS/ch2Fifo/sub_wire3
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/aclr
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/data
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/rdclk
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/rdreq
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/wrclk
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/wrreq
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/q
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/rdempty
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/rdusedw
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/wrfull
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/sub_wire0
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/sub_wire1
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/sub_wire2
add wave -noupdate -expand -group S -group eBusFifo3 /spi_test_top/topInstS/ch3Fifo/sub_wire3
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/aclr
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/data
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/rdclk
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/rdreq
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/wrclk
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/wrreq
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/q
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/rdempty
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/rdusedw
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/wrfull
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/sub_wire0
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/sub_wire1
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/sub_wire2
add wave -noupdate -expand -group S -group eBusFifo4 /spi_test_top/topInstS/ch4Fifo/sub_wire3
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/nRst
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/clk
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/ebus1FifoQ
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/ebus2FifoQ
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/ebusFifoEmpty
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/ebusFifoRdReq
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/eBusReq
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/reqFifoClr
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/pref1
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/pref2
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramWrData
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramAddr
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramRdReqW
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramWrReqW
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramRdData
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramRdValid
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramWaitReq
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramOutFifoQ
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramFifoRdReq
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramOutFifoE
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramFull
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoData
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoWrReq
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoAF
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoF
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoState
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoQ
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoE
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoRdReq
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/delay
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramState
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoERR
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramInFifoRdReqR
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdram2RdData
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdram2RdValid
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/wr_data
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramOutFifoAF
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/wordsToRead
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/ebusReqR
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramOutFifoWrusedw
add wave -noupdate -expand -group S -group sdramController1 /spi_test_top/topInstS/sdramCtrlInst1/sdramFifoF
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/nRst
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/clk
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/ebus1FifoQ
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/ebus2FifoQ
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/ebusFifoEmpty
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/ebusFifoRdReq
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/eBusReq
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/reqFifoClr
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/pref1
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/pref2
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramWrData
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramAddr
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramRdReqW
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramWrReqW
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramRdData
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramRdValid
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramWaitReq
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramOutFifoQ
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramFifoRdReq
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramOutFifoE
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramFull
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoData
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoWrReq
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoAF
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoF
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoState
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoQ
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoE
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoRdReq
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/delay
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramState
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoERR
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramInFifoRdReqR
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdram2RdData
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdram2RdValid
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/wr_data
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramOutFifoAF
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/wordsToRead
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/ebusReqR
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramOutFifoWrusedw
add wave -noupdate -expand -group S -group sdramController2 /spi_test_top/topInstS/sdramCtrlInst2/sdramFifoF
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/nrst
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/clk
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/sck
add wave -noupdate -expand -group S -expand -group spiSlave -radix hexadecimal /spi_test_top/topInstS/spiSlave/miso
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/mosi
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/ss
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/recvdData
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/dataRecvd
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/nLockBus
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/ebusFifoFull
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/eBusCh1Usedw
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/eBusCh2Usedw
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/eBusCh3Usedw
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/eBusCh4Usedw
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/SSELr
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/SSEL_active
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/SSEL_startmessage
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/SSEL_endmessage
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/spiEdgeCount
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/sendWord
add wave -noupdate -expand -group S -expand -group spiSlave /spi_test_top/topInstS/spiSlave/sendState
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {1415532 ps} 1} {{Cursor 2} {1079907754 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 315
configure wave -valuecolwidth 72
configure wave -justifyvalue left
configure wave -signalnamewidth 2
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 6
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1079729432 ps} {1080355541 ps}
