
`timescale 1 ns/ 1 ns
module spi_test_top;
parameter testVar = 0;
reg [5:0] counter=0;
reg reset, clk168=1, /*clk60=1,*/ clk56=1, clk28=1, ftClk=1, clk14=1;
reg [17:0] QB_ROUT;
reg QB_nLOCK = 0;
wire QB_RCLK = clk28;
wire QB_TCLK;
wire [17:0] QB_DIN;

reg usbnRxf=1, usbnTx=1;
wire [7:0] usbFtOut;
reg [7:0] usbFtR;
wire [7:0] usbFtin;
wire usbnRd, usbnOe;
wire [7:0] usbFtD = !usbnOe? usbFtR : usbFtOut;
reg [17:0] CH1=18'h001ff;
reg [17:0] CH2=18'h001ff;
reg [17:0] CH3=18'h001ff;

reg [17:0] CH5=18'h001ff;
reg [17:0] CH6=18'h001ff;
reg [17:0] CH7=18'h001ff;
reg [17:0] CH8=18'h001ff;

wire ch1Rclk = clk28;
reg caruClk=1;

wire qspi_clk, qspi_mosi, qspi_ss;
wire [3:0] qspi_miso;

wire m_sdram1_cke, m_sdram1_clk, m_sdram1_cs, m_sdram1_cas, m_sdram1_ras, m_sdram1_we;
wire [1:0] m_sdram1_ba;
wire [1:0] m_sdram1_dqm;
wire [12:0] m_sdram1_a;
wire [15:0] m_sdram1_d;

wire m_sdram2_cke, m_sdram2_clk, m_sdram2_cs, m_sdram2_cas, m_sdram2_ras, m_sdram2_we;
wire [1:0] m_sdram2_ba;
wire [1:0] m_sdram2_dqm;
wire [12:0] m_sdram2_a;
wire [15:0] m_sdram2_d;

reg TIM1r=0, TIM2r=0;

top topInstM(.CLOCK_AD9510_56(clk56), .CLOCK_LOC(clk56), .ID(0),
            .QB_DEN(), .QB_DIN(QB_DIN), .QB_TCLK(QB_TCLK), .QB_ROUT(QB_ROUT), .QB_RCLK(QB_RCLK), .QB_nLOCK(QB_nLOCK),
            .CH1(CH1), .CH1_RCLK(ch1Rclk), .CH1_nLock(0), 
            .CH2(CH2), .CH2_RCLK(ch1Rclk), .CH2_nLock(0), 
            .CH3(CH3), .CH3_RCLK(ch1Rclk), .CH3_nLock(0), .CH4_nLock(0),
            .USB_FT_CLKIN(ftClk), .USB_FT_nRXF(usbnRxf), .USB_FT_nOE(usbnOe), .USB_FT_nRD(usbnRd), .USB_FT_nTXE(usbnTx), .USB_FT_nWR() ,.USB_FT_D(usbFtD),
            .CARU_C_IN(caruClk), /*.CARU_D_IN(),*/ .CARU_C_OUT(), .CARU_D_OUT(),
            .TIM1(TIM1r), .TIM2(TIM2r),
            .CYC_SPI_SCK(qspi_clk), .CYC_SPI_MISO(qspi_miso), .CYC_SPI_MOSI(qspi_mosi), .CYC_SPI_SS(qspi_ss),
            .SDRAM1_BA(m_sdram1_ba), .SDRAM1_A(m_sdram1_a), .SDRAM1_CKE(m_sdram1_cke), .SDRAM1_CLK(m_sdram1_clk), 
            .SDRAM1_CS(m_sdram1_cs), .SDRAM1_CAS(m_sdram1_cas), .SDRAM1_RAS(m_sdram1_ras),
            .SDRAM1_WE(m_sdram1_we), .SDRAM1_DQM(m_sdram1_dqm), .SDRAM1_D(m_sdram1_d),
            
            .SDRAM2_BA(m_sdram2_ba), .SDRAM2_A(m_sdram2_a), .SDRAM2_CKE(m_sdram2_cke), .SDRAM2_CLK(m_sdram2_clk), 
            .SDRAM2_CS(m_sdram2_cs), .SDRAM2_CAS(m_sdram2_cas), .SDRAM2_RAS(m_sdram2_ras),
            .SDRAM2_WE(m_sdram2_we), .SDRAM2_DQM(m_sdram2_dqm), .SDRAM2_D(m_sdram2_d));

mt48lc32m16a2 m_sdram1(.Dq(m_sdram1_d), .Addr(m_sdram1_a), .Ba(m_sdram1_ba), 
                        .Clk(m_sdram1_clk), .Cke(m_sdram1_cke), .Cs_n(m_sdram1_cs), 
                        .Ras_n(m_sdram1_ras), .Cas_n(m_sdram1_cas), .We_n(m_sdram1_we), .Dqm(m_sdram1_dqm));

mt48lc32m16a2 m_sdram2(.Dq(m_sdram2_d), .Addr(m_sdram2_a), .Ba(m_sdram2_ba), 
                        .Clk(m_sdram2_clk), .Cke(m_sdram2_cke), .Cs_n(m_sdram2_cs), 
                        .Ras_n(m_sdram2_ras), .Cas_n(m_sdram2_cas), .We_n(m_sdram2_we), .Dqm(m_sdram2_dqm));

reg [3:0] chNLock=0;
top topInstS(.CLOCK_AD9510_56(clk56), .CLOCK_LOC(clk56), .ID(1),
            //.QB_DEN(), .QB_DIN(QB_DIN), .QB_TCLK(QB_TCLK), .QB_ROUT(QB_ROUT), .QB_RCLK(QB_RCLK), .QB_nLOCK(QB_nLOCK),
            .CH1(CH5), .CH1_RCLK(ch1Rclk), .CH1_nLock(0), 
            .CH2(CH6), .CH2_RCLK(ch1Rclk), .CH2_nLock(0), 
            .CH3(CH7), .CH3_RCLK(ch1Rclk), .CH3_nLock(0), 
            .CH4(CH8), .CH4_RCLK(ch1Rclk), .CH4_nLock(0),
            //.CH1(QB_DIN), .CH1_RCLK(QB_TCLK), .CH1_nLock(chNLock[0]), 
            //.CH2_nLock(chNLock[1]), .CH3_nLock(chNLock[2]), .CH4_nLock(chNLock[3]),
            
            /*
            .USB_FT_CLKIN(ftClk), .USB_FT_nRXF(usbnRxf), .USB_FT_nOE(usbnOe), .USB_FT_nRD(usbnRd), .USB_FT_nTXE(), .USB_FT_nWR() ,.USB_FT_D(usbFtD),
            .CARU_C_IN(caruClk), .CARU_D_IN(), .CARU_C_OUT(), .CARU_D_OUT()*/
            .CYC_SPI_SCK(qspi_clk), .CYC_SPI_MISO(qspi_miso), .CYC_SPI_MOSI(qspi_mosi), .CYC_SPI_SS(qspi_ss));
            
                                      
                  
                  
always
  #6 clk168 = ~clk168;
always
  #1 counter = counter+1;
//always 
//  #17 clk60 = ~clk60;
always 
  #17 clk56 = ~clk56;
always 
  #36 clk28 = ~clk28;
always
  #16 ftClk = ~ftClk;
always
  #71 clk14 =~clk14;
  
integer t;
initial begin
   #150000
   forever  begin  //TIM length 10mks, period 10,24ms
      TIM1r = 1;
      #10000
      TIM1r = 0;
      #10230000;
   end
   
end
   
initial begin
   #255000
   forever  begin  //TIM length 10mks, period 10,24ms
      TIM2r = 1;
      #10000
      TIM2r = 0;
      #10230000;
   end
end
  
initial
begin

//$display("===================== ");
//$display("%x", $random);
//$display(testVar);
//$display("===================== ");

  reset = 0;
  #10
  reset = 1;
  
  //#30
  //usbFifoFull = 1;
  //#20
  //usbFifoFull = 0;
  //forever begin
  //end
  //#100
  //$finish
  
end

reg [7:0] usbTestDataReqState [0:2];

initial
begin
   #1000 
   chNLock = 4'hf;
   #100000 
   chNLock = 4'h1;
   #100000
   chNLock = 4'h2;
   #100000 
   chNLock = 4'h3;
   
end

reg [17:0] qBusRxTestData [0:1024];
integer k;
initial begin
   qBusRxTestData[0] = 18'h001ff;
   qBusRxTestData[1] = 18'h001ff;
   qBusRxTestData[2] = 18'h001ff;
   
   qBusRxTestData[3] = 18'h16005;
   qBusRxTestData[4] = 18'h38004;
   qBusRxTestData[5] = 18'h38006;
   qBusRxTestData[6] = 18'h38008;
   qBusRxTestData[7] = 18'h38024;
   qBusRxTestData[8] = 18'h38026;
   qBusRxTestData[9] = 18'h21f9e;
   
   qBusRxTestData[10] = 18'h001ff;
   qBusRxTestData[11] = 18'h001ff;
   
   qBusRxTestData[12] = 18'h16000;
   qBusRxTestData[13] = 18'h38002;
   qBusRxTestData[14] = 18'h20020;
   qBusRxTestData[15] = 18'h38006;
   qBusRxTestData[16] = 18'h20000;
   qBusRxTestData[17] = 18'h38008;
   qBusRxTestData[18] = 18'h20000;
   qBusRxTestData[19] = 18'h38024;
   qBusRxTestData[20] = 18'h20000;
   qBusRxTestData[21] = 18'h38026;
   qBusRxTestData[22] = 18'h20000;
   qBusRxTestData[23] = 18'h38030;
   qBusRxTestData[24] = 18'h20000;
   qBusRxTestData[25] = 18'h001ff;
   
   QB_ROUT = 17'h001ff;
   #1000 
   @(posedge QB_RCLK)
   QB_ROUT = 17'hab000;
   @(posedge QB_RCLK)
   QB_ROUT = 17'h001ff;   
   //repeat (20) begin
   @(posedge QB_RCLK)
   @(posedge QB_RCLK)
   @(posedge QB_RCLK)
   @(posedge QB_RCLK)
   @(posedge QB_RCLK)
   @(posedge QB_RCLK)
   //end
   QB_ROUT = 17'h001ff;
   #1000 
   QB_ROUT = 17'hab000;   
   repeat (20) begin
      @(posedge QB_RCLK)
      QB_ROUT = QB_ROUT + 1;
   end
   #1 
   QB_ROUT = 18'h001ff;
   #600
   QB_ROUT = 17'hac000;   
   repeat (20) begin
      @(posedge QB_RCLK)
      QB_ROUT = QB_ROUT + 1;
   end
   #1 
   QB_ROUT = 17'h001ff;
   repeat (20) begin
      @(posedge QB_RCLK)        
      QB_ROUT = 17'h001ff;
   end
            
   for(k=0; k<26; k =k+1) begin
      @(posedge QB_RCLK)      begin  
         QB_ROUT = qBusRxTestData[k];
      end
   end
   
        
   
end

reg [7:0] usbTestData1 [0:1024];
reg [7:0] usbTestData2 [0:1024];
reg [7:0] usbTestData3 [0:1024];
reg [7:0] usbTestData4 [0:1024];
reg [7:0] usbTestData5 [0:1024];
reg [7:0] usbTestData6 [0:1024];
reg [7:0] usbTestData7 [0:1024];
reg [7:0] usbTestData8 [0:1024];
reg [7:0] usbTestData9 [0:1024];


integer i;
initial begin
   usbTestDataReqState[0] = 8'hbe;
   usbTestDataReqState[1] = 8'hef;
   usbTestDataReqState[2] = 8'hcc;
   
   usbTestData1[0] = 8'hbe;
   usbTestData1[1] = 8'hef;
   usbTestData1[2] = 8'hbb;
   usbTestData1[3] = 8'h00;
   usbTestData1[4] = 8'h15;
   
   
   usbTestData1[5] = 8'h01;
   usbTestData1[6] = 8'h60;
   usbTestData1[7] = 8'h05;
      
   usbTestData1[8] = 8'h03;
   usbTestData1[9] = 8'h80;
   usbTestData1[10] = 8'h04;
      
   usbTestData1[11] =  8'h03;
   usbTestData1[12] =  8'h80;
   usbTestData1[13] =  8'h06;
      
   usbTestData1[14] =  8'h03;
   usbTestData1[15] =  8'h80;
   usbTestData1[16] =  8'h08;
      
   usbTestData1[17] =  8'h03;
   usbTestData1[18] =  8'h80;
   usbTestData1[19] =  8'h24;

   usbTestData1[20] =  8'h03;
   usbTestData1[21] =  8'h80;
   usbTestData1[22] =  8'h26;

   usbTestData1[23] =  8'h02;
   usbTestData1[24] =  8'h1f;
   usbTestData1[25] =  8'h9e;

   
   usbTestData2[0] = 8'hbe;
   usbTestData2[1] = 8'hef;
   usbTestData2[2] = 8'haa;
   usbTestData2[3] = 8'h80;
   usbTestData2[4] = 8'h4e;
   usbTestData2[5] = 8'h00;
   
   usbTestData2[6] = 8'hbe;
   usbTestData2[7] = 8'hef;
   usbTestData2[8] = 8'haa;
   usbTestData2[9] = 8'h01;
   usbTestData2[10] = 8'h3c;
   usbTestData2[11] = 8'h08;
   
   usbTestData2[12] = 8'hbe;
   usbTestData2[13] = 8'hef;
   usbTestData2[14] = 8'haa;
   usbTestData2[15] = 8'h00;
   usbTestData2[16] = 8'h49;
   usbTestData2[17] = 8'h80;

   usbTestData2[18] = 8'hbe;
   usbTestData2[19] = 8'hef;
   usbTestData2[20] = 8'haa;
   usbTestData2[21] = 8'h00;
   usbTestData2[22] = 8'h49;
   usbTestData2[23] = 8'h80;

   
   usbTestData4[0] = 8'hbe;
   usbTestData4[1] = 8'hef;
   usbTestData4[2] = 8'hbb;
   usbTestData4[3] = 8'h00;
   usbTestData4[4] = 8'd15;
   
   usbTestData4[5] = 8'h01;
   usbTestData4[6] = 8'h50;
   usbTestData4[7] = 8'hd6;
   usbTestData4[8] = 8'h03;
   usbTestData4[9] = 8'h80;
   usbTestData4[10] = 8'h01;
   usbTestData4[11] = 8'h02;
   usbTestData4[12] = 8'h00;
   usbTestData4[13] = 8'h00;
   
   usbTestData4[14] = 8'h03;
   usbTestData4[15] = 8'h80;
   usbTestData4[16] = 8'h03;
   usbTestData4[17] = 8'h02;
   usbTestData4[18] = 8'h00;
   usbTestData4[19] = 8'h00;

   usbTestData5[0] = 8'hbe;
   usbTestData5[1] = 8'hef;
   usbTestData5[2] = 8'haa;
   usbTestData5[3] = 8'h02;
   usbTestData5[4] = 8'h3c;
   usbTestData5[5] = 8'h08;

   usbTestData6[0] = 8'hbe;
   usbTestData6[1] = 8'hef;
   usbTestData6[2] = 8'haa;
   usbTestData6[3] = 8'h03;
   usbTestData6[4] = 8'h3c;
   usbTestData6[5] = 8'h08;

   usbTestData7[0] = 8'hbe;
   usbTestData7[1] = 8'hef;
   usbTestData7[2] = 8'haa;
   usbTestData7[3] = 8'h04;
   usbTestData7[4] = 8'h3c;
   usbTestData7[5] = 8'h08;   

   usbTestData8[0] = 8'hbe;
   usbTestData8[1] = 8'hef;
   usbTestData8[2] = 8'haa;
   usbTestData8[3] = 8'h05;
   usbTestData8[4] = 8'h3c;
   usbTestData8[5] = 8'h08;      
   
   #1000   
   usbTestData9[0] = 8'hbe;
   usbTestData9[1] = 8'hef;
   usbTestData9[2] = 8'hbb;
   usbTestData9[3] = 8'h00;
   usbTestData9[4] = 8'h06;
   usbTestData9[5] = 8'h01;
   usbTestData9[6] = 8'h00;
   usbTestData9[7] = 8'h00;
   usbTestData9[8] = 8'h02;
   usbTestData9[9] = 8'hff;
   usbTestData9[10] = 8'hff;   
   
   
   
/*    @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<26; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData1[i];
   end
   usbnRxf = 1; 
   #1000*/
   

   #1000

   usbTestData1[3] = 8'h2;     //8'h2;   
   usbTestData1[4] = 8'h88; //8'h88;   
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<4; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData1[i];
   end
   
   for(i=0; i<649; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      if((i%3)==0) begin
         usbFtR = i/3;
      end
      else begin
         usbFtR = 0;
      end
      
   end
   usbnRxf = 1;
   #5000
   
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<4; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData1[i];
   end
   
   for(i=0; i<649; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      if((i%3)==0) begin
         usbFtR = i/3;
      end
      else begin
         usbFtR = 0;
      end
      
   end
   usbnRxf = 1;
   
   #100
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hde;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hdf;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;
   
   #10000
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<9; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData2[i];
   end
   usbnRxf = 1;
   #100  
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=9; i<24; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData2[i];
   end
   usbnRxf = 1;

   #30000
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hea;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1; 
   
   /*
   #10000
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hcc;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;
   */
   
   

  /*
   
   #1200   
   usbTestData9[0] = 8'hbe;
   usbTestData9[1] = 8'hef;
   usbTestData9[2] = 8'he8;
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData9[i];
   end
   usbnRxf = 1;
   
   usbTestData9[0] = 8'hbe;
   usbTestData9[1] = 8'hef;
   usbTestData9[2] = 8'he1;
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData9[i];
   end
   usbnRxf = 1;
   
   usbTestData9[0] = 8'hbe;
   usbTestData9[1] = 8'hef;
   usbTestData9[2] = 8'he2;
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData9[i];
   end
   usbnRxf = 1;
   usbTestData9[0] = 8'hbe;
   usbTestData9[1] = 8'hef;
   usbTestData9[2] = 8'he3;
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData9[i];
   end
   usbnRxf = 1;
   
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<6; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData1[i];
   end
   usbnRxf = 1;
   

   
   
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<20; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData1[i];
   end
   @(posedge ftClk)
   usbnRxf = 1;
   

   


   
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<20; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData4[i];
   end
   usbnRxf = 1;  
    
    
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;*/
/*    
   QB_nLOCK = 0;
   

   
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hcc;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;
   
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hdd;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;
   
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<6; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData5[i];
   end
   usbnRxf = 1;
   
   QB_nLOCK = 1;
   
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<6; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData6[i];
   end
   usbnRxf = 1;
   
   
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<6; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData7[i];
   end
   usbnRxf = 1;
   
   
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<6; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData8[i];
   end
   usbnRxf = 1;

   
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<20; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData4[i];
   end
   usbnRxf = 1;  */

   //#10000

   //send req
/*   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hcc;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;
   
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hdd;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;
   
      
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hee;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;*/
   
   #10000
   
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hdd;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;

   
   #10000
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbTestDataReqState[2] = 8'hcc;
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestDataReqState[i];
   end
   usbnRxf = 1;
   
   #391000

   usbTestData9[0] = 8'hbe;
   usbTestData9[1] = 8'hef;
   usbTestData9[2] = 8'he0;
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData9[i];
   end
   usbnRxf = 1;
   #300000

   usbTestData9[0] = 8'hbe;
   usbTestData9[1] = 8'hef;
   usbTestData9[2] = 8'he1;
   @(posedge ftClk)
   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<3; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData9[i];
   end
   usbnRxf = 1;

   //****
    
/*    @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   for(i=0; i<6; ) begin
      @(posedge ftClk) 
      if(!usbnRd) begin 
         i = i+1;
      end
      usbFtR = usbTestData7[i];
   end
   usbnRxf = 1; */
   

   
/*    @(posedge ftClk)
   @(posedge ftClk)
                    usbnRxf = 0;
   @(posedge !usbnRd) 
   @(posedge ftClk) usbFtR = 8'hbe;
   @(posedge ftClk) usbFtR = 8'hef;
   @(posedge ftClk) usbFtR = 8'haa;
   @(posedge ftClk) usbFtR = 8'h00;
   @(posedge ftClk) usbFtR = 8'h3c;
   @(posedge ftClk) usbFtR = 8'h08;
   @(posedge ftClk) usbnRxf = 1; 

   @(posedge ftClk)
   @(posedge ftClk) usbnRxf = 0;
   @(posedge !usbnRd)
   @(posedge ftClk) usbFtR = 8'hbe;
   @(posedge ftClk)  usbFtR = 8'hef;
   @(posedge ftClk)  usbFtR = 8'haa;
   @(posedge ftClk)  usbFtR = 8'h00;
   @(posedge ftClk)  usbFtR = 8'h5a;
   @(posedge ftClk)  usbFtR = 8'h01;
   @(posedge ftClk)  usbnRxf = 1;

   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   @(posedge !usbnRd)
   @(posedge ftClk) usbFtR = 8'hbe;
   @(posedge ftClk)  usbFtR = 8'hef;
   @(posedge ftClk)  usbFtR = 8'haa;
   @(posedge ftClk)  usbFtR = 8'h00;
   @(posedge ftClk)  usbFtR = 8'h49;
   @(posedge ftClk)  usbFtR = 8'h80;
   @(posedge ftClk)  usbnRxf = 1; 

   @(posedge ftClk)
   @(posedge ftClk)
   usbnRxf = 0;
   @(posedge !usbnRd)
   @(posedge ftClk) usbFtR = 8'hbe;
   @(posedge ftClk)  usbFtR = 8'hef;
   @(posedge ftClk)  usbFtR = 8'haa;
   @(posedge ftClk)  usbFtR = 8'h00;
   @(posedge ftClk)  usbFtR = 8'h5b;
   @(posedge ftClk)  usbFtR = 8'h80;
   @(posedge ftClk)  usbnRxf = 1;
   

   //#100
   @(posedge ftClk)
   usbnRxf = 0;
   @(posedge !usbnRd)
   @(posedge ftClk) usbFtR = 8'hbe;
   @(posedge ftClk) usbFtR = 8'hef;
   @(posedge ftClk) usbFtR = 8'hbb;
   @(posedge ftClk) usbFtR = 8'h00;
   @(posedge ftClk) usbFtR = 8'h1e;
   for(i=0; i<30; i=i+1)begin
      @(posedge ftClk) usbFtR = 8'h00;
      @(posedge ftClk) usbFtR = 8'h01;
      @(posedge ftClk) usbFtR = 8'h00+i;
   end
   @(posedge ftClk) usbnRxf = 1; */
   #500000
   repeat (1000) begin
      #1000
      @(posedge ftClk)
      usbTestDataReqState[2] = 8'hcc;
      usbnRxf = 0;
      for(i=0; i<3; ) begin
         @(posedge ftClk) 
         if(!usbnRd) begin 
            i = i+1;
         end
         usbFtR = usbTestDataReqState[i];
      end
      usbnRxf = 1;
   end

end

/* initial begin
   CH1 = 18'h001ff;
   #50 
   repeat (1000) begin
   @(posedge ch1Rclk);
   CH1 = 18'h10000;   
   @(posedge ch1Rclk);
   CH1 = 18'h2ffff;
   end   
   @(posedge ch1Rclk);
   CH1 = 18'h001ff;
end */

/* initial begin
   CH1 = 18'h001ff;
   CH5 = 18'h001ff;
   #50 
   @(posedge ch1Rclk);
   CH1 = 18'ha0a00;
   CH5 = 18'ha0500;   
   repeat (5) begin
      @(posedge ch1Rclk);
      CH1 = CH1 + 1;
      CH5 = CH5 + 1;
   end
   CH1 = 18'h001ff;
   CH5 = 18'h001ff;
   #450000 
   @(posedge ch1Rclk);
   CH1 = 18'ha1000;
   CH5 = 18'ha1000;
   repeat (50) begin
      @(posedge ch1Rclk);
      CH1 = CH1 + 1;
      CH5 = CH1 + 1;
   end
   @(posedge ch1Rclk);
   CH1 = 18'h001ff;
   CH5 = 18'h001ff;
end

initial begin
   CH2 = 18'h001ff;
   CH6 = 18'h001ff;
   #50 
   @(posedge ch1Rclk);
   CH2 = 18'ha0b00;   
   CH6 = 18'ha0600;   
   repeat (5) begin
      @(posedge ch1Rclk);
      CH2 = CH2 + 1;
      CH6 = CH6 + 1;
   end
   CH2 = 18'h001ff;
   CH6 = 18'h001ff;
   #450000 
   @(posedge ch1Rclk);
   CH2 = 18'ha1000;
   CH6 = 18'ha1000;
   @(posedge ch1Rclk);
   CH2 = 18'ha2fff;
   CH6 = 18'ha2fff;
   @(posedge ch1Rclk);
   CH2 = 18'h001ff;
   CH6 = 18'h001ff;
end 

initial begin
   CH3 = 18'h001ff;
   CH7 = 18'h001ff;
   CH8 = 18'h001ff;
   #50 
   @(posedge ch1Rclk);
   CH3 = 18'ha0c00;
   CH7 = 18'ha0700;
   CH8 = 18'ha0800;
   repeat (5) begin
      @(posedge ch1Rclk);
      CH3 = CH3 + 1;
      CH7 = CH7 + 1;
      CH8 = CH8 + 1;
   end
   CH3 = 18'h001ff;
   CH7 = 18'h001ff;
   CH8 = 18'h001ff;
   #450000 
   @(posedge ch1Rclk);
   CH3 = 18'ha1000;
   CH7 = 18'ha1000;
   CH8 = 18'ha1000;
   @(posedge ch1Rclk);
   CH3 = 18'ha2fff;
   CH7 = 18'ha2fff;
   CH8 = 18'ha2fff;
   @(posedge ch1Rclk);
   CH3 = 18'h001ff;
   CH7 = 18'h001ff;
   CH8 = 18'h001ff;
end  */

initial begin
   #100
   repeat (64) begin
      #17 caruClk = ~caruClk;
   end
end


always begin
   #50
   @(posedge ftClk)
   usbnTx=1;
   #35
   @(posedge ftClk)
   usbnTx=0;
   
end

endmodule
