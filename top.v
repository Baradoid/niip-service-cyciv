
module top(

	//////////// CLOCK //////////
	input  CLOCK_AD9510_56,
	input  CLOCK_LOC,
   output CLOCK_AD9510_OUT,
		
	//////////// COMMON PINS //////////
	input ID,
	output GPIO0,
	output GPIO1,
	//////////// SDRAM1 //////////
	//output nCEO_OUT,  
	//////////// SDRAM1 //////////
	output [1:0] SDRAM1_BA,
	output [12:0] SDRAM1_A,
	output SDRAM1_CKE,
	output SDRAM1_CLK,
	output SDRAM1_CS,
	output SDRAM1_CAS,
	output SDRAM1_RAS,
	output SDRAM1_WE,
	//output reg [1:0]  SDRAM1_DQM = 2'b11,
   output [1:0]  SDRAM1_DQM,
	inout  [15:0] SDRAM1_D,
	
	//////////// SDRAM2 //////////
   output [1:0] SDRAM2_BA,
	output [12:0] SDRAM2_A,
	output SDRAM2_CKE,
	output SDRAM2_CLK,
	output SDRAM2_CS,
	output SDRAM2_CAS,
	output SDRAM2_RAS,
	output SDRAM2_WE,
	//output reg [1:0]  SDRAM2_DQM=2'b00,
   output [1:0]  SDRAM2_DQM,
	inout  [15:0] SDRAM2_D,
	
	//////////// QBUS //////////
	output QB_DEN,
	output [17:0] QB_DIN,	
	output QB_TCLK,
	
	//output QB_REN,
	input [17:0] QB_ROUT,
	input QB_RCLK,
   input QB_nLOCK,
	
   input [17:0] CH1,
   input CH1_nLock,
   input CH1_RCLK,
   
   input [17:0] CH2,
   input CH2_nLock,
   input CH2_RCLK,
   
   input [17:0] CH3,
   input CH3_nLock,
   input CH3_RCLK,
   
   input [17:0] CH4,
   input CH4_nLock,
   input CH4_RCLK,


	//////////// USB_FT //////////
	//input [7:0] USB_FT_ACBUS,
	
	input USB_FT_CLKIN,
	
	input USB_FT_nRXF,
	//input USB_FT_nOE,
	output reg USB_FT_nOE = 1,
	output USB_FT_nRD ,
	
	input USB_FT_nTXE,
	output USB_FT_nWR,	
	
	inout [7:0] USB_FT_D,
	
	//output reg USB_FT_nPWRSAV = 1,
	//input USB_FT_nPWRSAV,
	input USB_FT_SIWU,			//******
	//input USB_FT_SIWU,
	
	//////////// USB_FT //////////
	//output 		 UC_CLK,
	//output		 UC_ALE,	
	//output [7:0] UC_P0,
	//output [7:0] UC_P1,
	//output [7:0] UC_P2,
	//output [7:0] UC_P3,	
	
	//////////// CYCIV //////////	
	inout 	   CYC_SPI_SCK,
	inout [3:0] CYC_SPI_MISO,
	inout 	   CYC_SPI_MOSI,	
	inout 	   CYC_SPI_SS,
	
	output	MAXV_CLOCK,
	//input 	MAXV_IRQ,	
	
		
	//////////// MAXV //////////	
	//output	I2C_CLK,
	//inout 	I2C_SDA,	
		
	//////////// CARU //////////	
	output 	CARU_EN,
	
	input 	CARU_C_IN,
	input 	CARU_D_IN,
	output 	CARU_C_OUT,
	output 	CARU_D_OUT,
	
	//////////// SPI //////////	
	//input		SPI_SCLK,	
	//output	SPI_MISO,
	//input		SPI_MOSI,
	//input		SPI_CS,
	
	//input		DS,
	//input  	TI,
	
	//////////// SIGNALS //////////	
	output X6_ENA,
	output X6_TAU_OBM,
	output X6_ISCHP,
	//output X6_ISCHP_OP,
	//output X6_KO,
	
	//////////// SIGNALS //////////	
	/*output X29_ENA,
	input X24_IBP,
	input X24_SYNTH_OK,
	input X24_MFS_OK,*/
	
	//////////// CONN20 //////////	
	input TIM1, //input CONN20_TIM1,
	input TIM2,
	//input CONN20_TIM3,
	input IZP,
	
	
	//////////// SIGNALS //////////	
	//output PRD_OK,
	//output VIP_OK,
	//output TR_OK,
	//output NADD1_OK,
	//output MOD_OK,
	//output P3_OK,
	//output P1_OK,
	//output POUT_OK,
	
	//////////// SIGNALS //////////	
	/*output X27_ENA,
	input X27_ADC1_OL,
	input X27_ADC2_OL,
	input X27_ADC3_OL,	
	input X27_ADC4_OL,

	input X27_ADC1_OK,
	input X27_ADC2_OK,
	input X27_ADC3_OK,	
	input X27_ADC4_OK,
	
	input X27_SHU_OK,
	input X27_PS_OK,
	input X27_SOCHI_OK,
	input X27_CARU_OK,
	input X27_IP32_OK,
	
	input X27_DET41,
	input X27_DET42,
	input X27_DET43,
	input X27_DET44,
	
	input X27_DET31,
	input X27_DET32,
	input X27_DET33,
	input X27_DET34,
	
	input X27_DET21,
	input X27_DET22,
	input X27_DET23,
	input X27_DET24,

	input X27_DET11,
	input X27_DET12,
	input X27_DET13,
	input X27_DET14,*/
	
	//////////// SIGNALS //////////	
	//output X8_X11_ENA,
	//output X8_IV,
	//output X11_D0,
	
	// AD9510 SPI
	output AD9510_SCLK,
	output AD9510_SDIO,
	input AD9510_SDO,
	output AD9510_SS,

	output [7:0] caruRamQ,
	output [3:0] caruRamRdAddr,
	output [7:0] debCaruDataByte,
   output [3:0] protoParserState
   
   //output QBDIN9_X29ENA
);

//assign nCEO_OUT = 0;
wire isMaster  = (ID==0);
wire isSlave = (ID==1);

wire CLOCK_PLL, clk28, CLOCK_PLL_112, CLOCK_PLL_128, CLOCK_PLL_14  /* synthesis keep */;
wire CLOCK_56 = CLOCK_LOC;
wire clk168 = CLOCK_PLL;
wire clkInt = CLOCK_PLL_112; //CLOCK_PLL_128;
assign CLOCK_AD9510_OUT = CLOCK_56; //clk28;
wire reset;
wire nRst = reset;

wire CLOCK_2MHZ;
pll	pllInst(.inclk0(/*CLOCK_LOC*/CLOCK_56) ,.c0(CLOCK_PLL_112), .c1(clk28), .c2(CLOCK_PLL_128), .c3(CLOCK_PLL_14), .c4(CLOCK_2MHZ), .locked(reset));
//assign clk28 = CLOCK_50;
reg [15:0] count = 0;
initial count <= 32'b0;
reg [1:0] ind = 0;

assign MAXV_CLOCK = CLOCK_LOC;

reg [1:0] clockDel = 0;
always @(posedge CLOCK_LOC) clockDel<=clockDel+1;

reg TIM1024=0; 
reg [31:0] TIMclkCounter=0;
reg [2:0] TIMState=0;
always @(posedge CLOCK_56 or negedge reset) begin
   if(~reset) begin
      TIMclkCounter <= 0;
      TIMState <= 0;
      TIM1024 <= 0;
   end
   else begin
      if(TIMState==0) begin
         if(TIMclkCounter > 6017640) begin  //601764 - 10230mks
            TIMState <= 1;
            TIMclkCounter <= 0;
            TIM1024 <= 1;
         end
         else begin
            TIMclkCounter <= TIMclkCounter + 1;
         end
      end
      else if(TIMState[0]) begin        //10mks
         if(TIMclkCounter > 588) begin 
            TIMState <= 0;
            TIMclkCounter <= 0;
            TIM1024 <= 0;
         end
         else begin
            TIMclkCounter <= TIMclkCounter + 1;
         end
      end      
   end
end

reg TIM1s=0; 
reg [31:0] TIM1sclkCounter=0;
reg [2:0] TIM1sState=0;
always @(posedge CLOCK_56 or negedge reset) begin
   if(~reset) begin
      TIM1sclkCounter <= 0;
      TIM1sState <= 0;
      TIM1s <= 0;
   end
   else begin
      if(TIM1sState==0) begin
         if(TIM1sclkCounter > 60176400) begin  //601764 - 10230mks
            TIM1sState <= 1;
            TIM1sclkCounter <= 0;
            TIM1s <= 1;
         end
         else begin
            TIM1sclkCounter <= TIM1sclkCounter + 1;
         end
      end
      else if(TIM1sState[0]) begin        //10mks
         if(TIM1sclkCounter > 588) begin 
            TIM1sState <= 0;
            TIM1sclkCounter <= 0;
            TIM1s <= 0;
         end
         else begin
            TIM1sclkCounter <= TIM1sclkCounter + 1;
         end
      end      
   end
end


assign GPIO0 = TIM1s; //(ID==0) ? CLOCK_LOC : clockDel[1]; //USB_FT_CLKIN;
assign GPIO1 = TIM1024;

assign CLOCK_SYNC = 1;

assign QB_DEN = 1;

reg clkDelim;


assign X6_ENA = 0;

reg [31:0] clkCnt=0; always @(posedge CLOCK_LOC) clkCnt <= clkCnt + 1;

assign X6_TAU_OBM = clkCnt[16];
assign X6_ISCHP = clkCnt[16];
assign X6_ISCHP_OP  = clkCnt[16];


//mixex pins
wire QBDIN9 ;
wire X29ENA = 1;

//assign QBDIN9_X29ENA = (ID==0) ? QBDIN9 : X29ENA;

//----- USB

wire ftClk = USB_FT_CLKIN;
//assign ftClckEdgeDet = (~USB_FT_CLKINr[0] & USB_FT_CLKIN) ||(~USB_FT_CLKINr[1] & USB_FT_CLKINr[0]);

reg [1:0] USB_FT_CLKINr=0; always @(posedge CLOCK_PLL) USB_FT_CLKINr<={USB_FT_CLKINr[0], USB_FT_CLKIN};

//assign ftClckEdgeDetPin = ftClckEdgeDet ;


reg USB_FT_nRxfD = 0;
reg [7:0] rDBuf;
wire [15:0] fifoRdCounter;
assign fifoRdCounterPin = fifoRdCounter;

wire lbFifoEmpty, fifoLbRdReq/*, fifoLbWrReq*/;


wire [15:0] fifoUsbRxOut;
//reg [15:0] fifoUsbRxOutPinR;
//assign fifoUsbRxOutPin = fifoUsbRxOut;
//wire [8:0] fifoUsbRxWrusedw;
//assign fifoUsbRxWrusedwPin = fifoUsbRxWrusedw;

wire fifoUsbRxEmpty;
wire fifoUsbTxRdEmpty;
reg fifoUsbRxEmptyD=1; always @(posedge clk28) fifoUsbRxEmptyD<=fifoUsbRxEmpty;
reg [511:0] fifoUsbRxRdReqR=0;  always @(posedge clk28)  fifoUsbRxRdReqR <= {fifoUsbRxRdReqR[510:0],  !fifoUsbRxEmpty};
wire fifoUsbRxRdReq = fifoUsbRxRdReqR[511]&&(!fifoUsbRxEmpty);

//reg [1:0] msb = 0; always @(posedge ftClk) if((USB_FT_nRD==1) && (USB_FT_nOE==0)) msb <= msb + 1;


wire usbRx88FF;
reg USB_FT_nRDr = 1;
//reg [1:0] recvByteCount=0; 
wire usbRdAllow = !USB_FT_nRDr&&!USB_FT_nRXF&&!usbRx88FF;
//wire fifoLbWrReq= usbRdAllow && (recvByteCount==2);
//reg [7:0] fifoUsbRxDataR=8'd0; 
//reg [15:0] fifoUsbRxDataR;
integer resetCounter=0;

assign USB_FT_nRD  = USB_FT_nRDr || usbRx88FF;
always @(posedge ftClk) begin
	if(USB_FT_nRXF)
		resetCounter <= resetCounter + 1;
	else 
		resetCounter <= 0;
	
	//if(resetCounter > 32'h8000000) //>2s
	//	recvByteCount <= 0;
	
	
	USB_FT_nOE <= USB_FT_nRXF;	
	USB_FT_nRDr <= USB_FT_nOE || USB_FT_nRXF;
	
	//if(usbRdAllow) begin
		//recvByteCount <= (recvByteCount<2) ? recvByteCount + 1: 0;		
		//fifoUsbRxDataR[15:0] <= {fifoUsbRxDataR[7:0], USB_FT_D[7:0]};	
	//end
		
end

wire usbRx88FifoEmpty, usbRx88FifoRdReq /* synthesis noprune */;
wire [7:0] usbRx88FifoQ /* synthesis noprune */ ;
wire [24:0] fifoUseDw;
usbFifo88DualClk usbRxFifo88(.wrclk(ftClk),.wrreq(usbRdAllow),.wrfull(usbRx88FF),.data(USB_FT_D),
											.rdclk(clkInt), .rdempty(usbRx88FifoEmpty), .rdreq(usbRx88FifoRdReq), .q(usbRx88FifoQ), .rdusedw(fifoUseDw));

wire [23:0] usbProtoParserQ;
wire ad9510CtrlValid, ad9510CtrlReady;
wire qBusDataValid, qBusReady;
wire QbusTxWrFull;
wire qBusReadyToRecv;

wire stateReq, qBusReq, reqFifoClr, reqParamsSend;
wire [7:0] eBusReq;

wire paramsFifoE, paramsFifoF;
wire paramsUpdateStart, paramsUpdateEnd;
wire sendParamsOnTim2;
wire eBusTestMode;

UsbProtoParser UsbProtoParserInst(.nrst(reset&isMaster), .clk(clkInt), .data(usbRx88FifoQ), .fifoRdReq(usbRx88FifoRdReq), .fifoEmpty(usbRx88FifoEmpty), .fifoUsedw(fifoUseDw),
                                 .q(usbProtoParserQ), .SCQvalid(ad9510CtrlValid), .SCQready(ad9510CtrlReady),
                                 .qBusReady(qBusReadyToRecv /*!QbusTxWrFull*/), .qBusDataValid(qBusDataValid), .updateParamsStart(paramsUpdateStart), .updateParamsEnd(paramsUpdateEnd),
                                 .reqParamsSend(reqParamsSend), .sendParamsOnTim2(sendParamsOnTim2),
                                 .stateReq(stateReq), .qBusReq(qBusReq), .eBusReq(eBusReq), .reqFifoClr(reqFifoClr),
                                 .caru_en(CARU_EN), .stateW(protoParserState), .eBusTestMode(eBusTestMode));
                             
//AD9510 control
wire ad9510DataReady, ad9510DataRdAck;
wire [7:0] ad9510Data;
ad9510_ctrl ad9510_ctrl_inst(.nrst(reset&isMaster), .clk(clkInt), .AD9510_SCLK(AD9510_SCLK), .AD9510_SDIO(AD9510_SDIO), .AD9510_SDO(AD9510_SDO), .AD9510_SS(AD9510_SS),
                              .dataReady(ad9510DataReady), .dataRdAck(ad9510DataRdAck), .data(ad9510Data),
                              .dataToSend(usbProtoParserQ), .valid(ad9510CtrlValid), .ready(ad9510CtrlReady));     

// ======  ds92lv18 QBUS

//reg [20:0] QbusTxRdEmptyR = 0; always @(posedge QB_TCLK) QbusTxRdEmptyR <= {QbusTxRdEmptyR[19:0], QbusTxRdEmpty};
/* wire [17:0] paramsFifoQ;
wire paramsFifoRdReq;
paramsFifo paramsFifoInst(.aclr(reset), .clock(clkInt), .data(usbProtoParserQ[17:0]), .rdreq(), .wrreq(qBusDataValid), .empty(paramsFifoE), .full(paramsFifoF), .q(paramsFifoQ));*/


reg TIM1r=0; always @(posedge clkInt) TIM1r <= TIM1;
reg TIM2r=0; always @(posedge clkInt) TIM2r <= TIM2;

reg [31:0] TIM1ClkCounter=0; always @(posedge clkInt) TIM1ClkCounter <= (TIM1&~TIM1r)? 5600000: TIM1ClkCounter>0? TIM1ClkCounter-1 :0;
reg [31:0] TIM2ClkCounter=0; always @(posedge clkInt) TIM2ClkCounter <= (TIM2&~TIM2r)? 5600000: TIM2ClkCounter>0? TIM2ClkCounter-1 :0;

wire TIM1Locked = (TIM1ClkCounter!=0);
wire TIM2Locked = (TIM2ClkCounter!=0);

reg [5:0] paramsFSMState=0;
reg [7:0] paramsWrMemAddr=0;
reg [7:0] paramsRdMemAddr=0;
wire [7:0] paramsMemAddr = paramsFSMState[0]? paramsWrMemAddr : paramsRdMemAddr;
wire [7:0] paramsLen = paramsWrMemAddr;
reg [17:0] paramsMemDataLen=0;
wire paramsMemWrEn = paramsFSMState[0] && qBusDataValid;
assign qBusReadyToRecv = !paramsFSMState || paramsFSMState[0];
reg reqParamsSendR=0; always @(posedge clkInt) reqParamsSendR <= (!reset || paramsFSMState[1]) ? 0 : reqParamsSend|reqParamsSendR|(TIM2&!TIM2r&sendParamsOnTim2);
wire paramsRdValid = paramsFSMState[1] && (paramsRdMemAddr>0) && !QbusTxWrFull;

always @(posedge clkInt or negedge reset) begin
   //reqParamsSendR <= (!reset||paramsFSMState[1]) ? 0 : reqParamsSend|reqParamsSendR;
   
   if(~reset) begin
      paramsFSMState <= 0;
      paramsMemDataLen <= 0;
   end
   else begin
      if(!paramsFSMState) begin
         if(paramsUpdateStart) begin
            paramsFSMState <= 1;
            paramsWrMemAddr <= 0;
         end
         else if(reqParamsSendR) begin
            paramsFSMState <= 2;
            paramsRdMemAddr <= 0;
         end
      end
      else if(paramsFSMState[0]) begin
         if(qBusDataValid) begin
            paramsWrMemAddr <= paramsWrMemAddr + 1;
         end
         if(paramsUpdateEnd) begin
            paramsFSMState <= 0;
         end
      end
      else if(paramsFSMState[1]) begin
         if(paramsRdMemAddr < paramsLen) begin
            if(!QbusTxWrFull) begin
               paramsRdMemAddr <= paramsRdMemAddr + 1;
            end
         end
         else begin
            paramsFSMState <= 0;
         end
      end
   end
end

wire [17:0] paramsMemData;
paramsMem paramsMemInst(.aclr(!reset), .clock(clkInt), .address(paramsMemAddr), .data(usbProtoParserQ[17:0]), .wren(paramsMemWrEn), .q(paramsMemData));


//wire qBusTxFifoFull, qBusTxFifoEmpty;
//wire qBusTxFifoWrReq = qBusDataValid;
assign qBusReady = 1; //!qBusTxFifoFull;
wire QbusTxRdEmpty;
wire [17:0] qBusTxFifoOut;
assign QB_TCLK = clk28;


wire [9:0] qBusTxRdusedw;
reg [9:0] qBusTxRdusedwR=0; always @(posedge QB_TCLK) qBusTxRdusedwR <= qBusTxRdusedw;
reg [7:0] qBusTxRIdleCount=0; always @(posedge QB_TCLK) qBusTxRIdleCount <= ((qBusTxRdusedw==qBusTxRdusedwR) && !QbusTxRdEmpty)? qBusTxRIdleCount+1: 0;
reg [9:0] qBusTxSendCount = 0;
always @(posedge QB_TCLK) begin
   if((qBusTxRIdleCount==8'd10) && (qBusTxSendCount==0)) begin
      qBusTxSendCount <= qBusTxRdusedw;
   end
   else if(qBusTxSendCount>0) begin
      qBusTxSendCount   <= qBusTxSendCount -1;
   end
end

wire qBusFifoTxRdReq = !QbusTxRdEmpty;//qBusTxSendCount>0;
reg qBusFifoTxRdReqR=0; always @(posedge QB_TCLK) qBusFifoTxRdReqR <= qBusFifoTxRdReq;

fifo1818 fifo1818_QbusTx(.data(paramsMemData), .wrclk(clkInt), .wrreq(paramsRdValid), .wrfull(QbusTxWrFull),
							    .q(qBusTxFifoOut), .rdclk(QB_TCLK), .rdreq(qBusFifoTxRdReq), .rdempty(QbusTxRdEmpty), .rdusedw(qBusTxRdusedw)); 

//wire [17:0] QB_DINb = {QB_DIN[17:10], QBDIN9, QB_DIN[8:0]};
assign QB_DIN = (qBusFifoTxRdReqR) ? qBusTxFifoOut: 18'h001ff;      

wire qBUSfifoFull, qBUSfifoEmpty, qBusFifoRdReq; 
wire [17:0] qBusFifoOut;
reg qbnLockR=0; always @(posedge QB_RCLK) qbnLockR <= QB_nLOCK;
wire fifoQbusAclr = qbnLockR != QB_nLOCK;
wire qbusFifoWrReq = (QB_ROUT[17:0]!=18'h001ff) && !qBUSfifoFull && !QB_nLOCK /*&& (QB_ROUT[17:16]!=2'b11)*/;
wire [9:0] qBusUsedw;
fifo1818 fifo1818_QbusRx(.aclr(reqFifoClr), .data(QB_ROUT), .wrclk(QB_RCLK), .wrreq(qbusFifoWrReq), .wrfull(qBUSfifoFull),
//reg wrReqR=0; always @(posedge QB_TCLK) wrReqR<=!QbusTxRdEmpty;
//fifo1818 fifo1818_QbusRx( .data(qBusTxFifoOut), .wrclk(QB_TCLK), .wrreq(wrReqR), .wrfull(qBUSfifoFull),
							  .q(qBusFifoOut), .rdclk(clkInt), .rdreq(qBusFifoRdReq), .rdempty(qBUSfifoEmpty), .rdusedw(qBusUsedw)); 



wire [17:0] ebus1FifoQ, ebus2FifoQ, ebus3FifoQ, ebus4FifoQ;                       
wire [7:0] ebusFifoFull, ebusFifoEmpty, ebusFifoRdReq;
reg [31:0] clockCounter; always @(posedge CLOCK_56) clockCounter <= clockCounter + 1;
reg [1:0] clkD; always @(posedge CLOCK_56) clkD <= {clkD[0], clockCounter[10]};
wire ch12FifoWrReq = (clockCounter[10]==1)&&((clkD[0]==0)/*||(clkD[1]==0)*/);
reg [24:0] eBusChUsedw[0:7];
reg [24:0] eBusChUsedwIntClk1[0:7];
reg [24:0] eBusChUsedwIntClk[0:7];

reg [17:0] data = 18'h11111;
//assign ebus1FifoQ = data;
always @(posedge CH1_RCLK) begin
   //if(ebusFifoRdReq[0]) begin
   if( (CH1[17:0] != 18'h001ff) && !CH1_nLock ) begin
      data <= data + 1;
   end
end


always @(posedge CH1_RCLK or negedge nRst or posedge reqFifoClr) if (~nRst || reqFifoClr) eBusChUsedw[0] <= 0; else if(CH1[17:0] != 18'h001ff) eBusChUsedw[0] <= eBusChUsedw[0]+1;
always @(posedge CH2_RCLK or negedge nRst or posedge reqFifoClr) if (~nRst || reqFifoClr) eBusChUsedw[1] <= 0; else if(CH2[17:0] != 18'h001ff) eBusChUsedw[1] <= eBusChUsedw[1]+1;
always @(posedge CH3_RCLK or negedge nRst or posedge reqFifoClr) if (~nRst || reqFifoClr) eBusChUsedw[2] <= 0; else if(CH3[17:0] != 18'h001ff) eBusChUsedw[2] <= eBusChUsedw[2]+1;
always @(posedge CH4_RCLK or negedge nRst or posedge reqFifoClr) if (~nRst || reqFifoClr) eBusChUsedw[3] <= 0; else if(CH4[17:0] != 18'h001ff) eBusChUsedw[3] <= eBusChUsedw[3]+1; 

always @(posedge clkInt) begin eBusChUsedwIntClk[0] <= eBusChUsedwIntClk1[0]; eBusChUsedwIntClk1[0] <= eBusChUsedw[0];
                               eBusChUsedwIntClk[1] <= eBusChUsedwIntClk1[1]; eBusChUsedwIntClk1[1] <= eBusChUsedw[1];
                               eBusChUsedwIntClk[2] <= eBusChUsedwIntClk1[2]; eBusChUsedwIntClk1[2] <= eBusChUsedw[2];
                               eBusChUsedwIntClk[3] <= eBusChUsedwIntClk1[3]; eBusChUsedwIntClk1[3] <= eBusChUsedw[3]; end

reg [17:0] Ch1testData=0; always @(posedge CLOCK_56 or negedge nRst or posedge reqFifoClr)  if (~nRst || reqFifoClr) Ch1testData <= 0; else if(eBusTestMode) Ch1testData <= Ch1testData + 1;
reg [17:0] Ch2testData=0; always @(posedge CLOCK_56 or negedge nRst or posedge reqFifoClr)  if (~nRst || reqFifoClr) Ch2testData <= 0; else if(eBusTestMode) Ch2testData <= Ch2testData + 1;
reg [17:0] Ch3testData=0; always @(posedge CLOCK_56 or negedge nRst or posedge reqFifoClr)  if (~nRst || reqFifoClr) Ch3testData <= 0; else if(eBusTestMode) Ch3testData <= Ch3testData + 1;
reg [17:0] Ch4testData=0; always @(posedge CLOCK_56 or negedge nRst or posedge reqFifoClr)  if (~nRst || reqFifoClr) Ch4testData <= 0; else if(eBusTestMode) Ch4testData <= Ch4testData + 1;

wire ch1WrReq = eBusTestMode? 1 : (CH1[17:0] != 18'h001ff) && !CH1_nLock && !(ebusFifoFullR[0]);
wire ch2WrReq = eBusTestMode? 1 : (CH2[17:0] != 18'h001ff) && !CH2_nLock && !(ebusFifoFullR[1]);
wire ch3WrReq = eBusTestMode? 1 : (CH3[17:0] != 18'h001ff) && !CH3_nLock && !(ebusFifoFullR[2]);
wire ch4WrReq = eBusTestMode? 1 : (CH4[17:0] != 18'h001ff) && !CH4_nLock && !(ebusFifoFullR[3]);

reg [7:0] ebusFifoFullR=0; always @(posedge clkInt)  ebusFifoFullR <= ebusFifoFull;
wire ch1InClk = eBusTestMode? CLOCK_56:CH1_RCLK;
wire ch2InClk = eBusTestMode? CLOCK_56:CH2_RCLK;
wire ch3InClk = eBusTestMode? CLOCK_56:CH3_RCLK;
wire ch4InClk = eBusTestMode? CLOCK_56:CH4_RCLK;
//altClkCtrl altClkCtrl_inst1(.inclk1x(CH1_RCLK), .inclk0x(CLOCK_56), .clkselect(eBusTestMode), .outclk(ch1InClk));
//altClkCtrl altClkCtrl_inst2(.inclk1x(CLOCK_56), .inclk0x(CH2_RCLK), .clkselect(eBusTestMode), .outclk(ch2InClk));
//altClkCtrl altClkCtrl_inst3(.inclk1x(CLOCK_56), .inclk0x(CH3_RCLK), .clkselect(eBusTestMode), .outclk(ch3InClk));
//altClkCtrl altClkCtrl_inst4(.inclk1x(CLOCK_56), .inclk0x(CH4_RCLK), .clkselect(eBusTestMode), .outclk(ch4InClk));


ebusFifo ch1Fifo(.aclr(reqFifoClr), .data(eBusTestMode? Ch1testData : CH1), .wrclk(ch1InClk), .wrreq(ch1WrReq), .wrfull(ebusFifoFull[0]),
						.q(ebus1FifoQ), .rdclk(clkInt), .rdreq(ebusFifoRdReq[0]),  .rdempty(ebusFifoEmpty[0])/*, .rdusedw(eBusCh1Usedw)*/);
ebusFifo ch2Fifo(.aclr(reqFifoClr), .data(eBusTestMode? Ch2testData : CH2), .wrclk(ch2InClk), .wrreq(ch2WrReq), .wrfull(ebusFifoFull[1]),
						.q(ebus2FifoQ), .rdclk(clkInt), .rdreq(ebusFifoRdReq[1]),  .rdempty(ebusFifoEmpty[1])/*, .rdusedw(eBusCh2Usedw)*/);
ebusFifo ch3Fifo(.aclr(reqFifoClr), .data(eBusTestMode? Ch3testData : CH3), .wrclk(ch3InClk), .wrreq(ch3WrReq), .wrfull(ebusFifoFull[2]),
						.q(ebus3FifoQ), .rdclk(clkInt), .rdreq(ebusFifoRdReq[2]),  .rdempty(ebusFifoEmpty[2])/*, .rdusedw(eBusCh3Usedw)*/);
ebusFifo ch4Fifo(.aclr(reqFifoClr), .data(eBusTestMode? Ch4testData : CH4), .wrclk(ch4InClk), .wrreq(ch4WrReq), .wrfull(ebusFifoFull[3]),
						.q(ebus4FifoQ), .rdclk(clkInt), .rdreq(ebusFifoRdReq[3]),  .rdempty(ebusFifoEmpty[3])/*, .rdusedw(eBusCh4Usedw)*/); 
					
						
wire [7:0] sdram1OutFifoQ, sdram2OutFifoQ;
wire sdram1FifoRdReq, sdram2FifoRdReq;
wire sdram1OutFifoE, sdram2OutFifoE;
wire sdram1FifoF;

wire [15:0] sdram1WrData, sdram2WrData;
wire [15:0] sdram1Addr, sdram2Addr;
wire sdram1RdReqW, sdram2RdReqW; 
wire sdram1WrReqW, sdram2WrReqW;
wire [15:0] sdram1RdData, sdram2RdData;
wire sdram1RdValid, sdram2RdValid;
wire sdram1WaitReq, sdram2WaitReq;

wire sdram1Full, sdram2Full;

sdramController sdramCtrlInst1(.nRst(reset), .clk(clkInt),
                               .eBusReq(eBusReq[0]), .reqFifoClr(reqFifoClr),
                               .ebus1FifoQ(ebus1FifoQ), .ebus2FifoQ(ebus2FifoQ),
                               .ebusFifoRdReq(ebusFifoRdReq[1:0]), .ebusFifoEmpty(ebusFifoEmpty[1:0]),
                               
                               .pref1(ID==0?8'h11:8'h55), .pref2(ID==0?8'h22:8'h66),
                               
                               .sdramWrData(sdram1WrData), 
                               .sdramAddr(sdram1Addr[15:0]),
                               .sdramRdReqW(sdram1RdReqW), .sdramWrReqW(sdram1WrReqW),
                               .sdramRdData(sdram1RdData),
                               .sdramRdValid(sdram1RdValid), .sdramWaitReq(sdram1WaitReq),
                               
                               .sdramOutFifoQ(sdram1OutFifoQ), .sdramFifoRdReq(sdram1FifoRdReq), 
                               .sdramOutFifoE(sdram1OutFifoE), //.sdramFifoF(sdram1FifoF),
										 .sdramFull(sdram1Full)
                               );
                               
sdramController sdramCtrlInst2(.nRst(reset), .clk(clkInt),
                               .eBusReq(eBusReq[1]), .reqFifoClr(reqFifoClr),
                               .ebus1FifoQ(ebus3FifoQ), .ebus2FifoQ(ebus4FifoQ),
                               .ebusFifoRdReq(ebusFifoRdReq[3:2]), .ebusFifoEmpty(ebusFifoEmpty[3:2]),
                               
                               .pref1(ID==0?8'h33:8'h77), .pref2(ID==0?8'h44:8'h88),
                               
                               .sdramWrData(sdram2WrData), 
                               .sdramAddr(sdram2Addr[14:0]),
                               .sdramRdReqW(sdram2RdReqW), .sdramWrReqW(sdram2WrReqW),
                               .sdramRdData(sdram2RdData),
                               .sdramRdValid(sdram2RdValid), .sdramWaitReq(sdram2WaitReq),
                               
                               .sdramOutFifoQ(sdram2OutFifoQ), .sdramFifoRdReq(sdram2FifoRdReq), 
                               .sdramOutFifoE(sdram2OutFifoE), //.sdramFifoF(sdram1FifoF),
										 .sdramFull(sdram2Full)
                               );
//sdramController sdramCtrlInst2();

//assign CKE = 1;
//assign CS = 0;
assign SDRAM1_CLK = clkInt;
assign SDRAM2_CLK = clkInt;

sdram sdram_inst(.clk_clk(clkInt),
                 .reset_reset_n(nRst),
                 .sdram_controller_0_wire_addr(SDRAM1_A),    // new_sdram_controller_0_wire.addr
                 .sdram_controller_0_wire_ba(SDRAM1_BA),      //                            .ba
                 .sdram_controller_0_wire_cas_n(SDRAM1_CAS),   //                            .cas_n
                 .sdram_controller_0_wire_cke(SDRAM1_CKE),     //                            .cke
                 .sdram_controller_0_wire_cs_n(SDRAM1_CS),    //                            .cs_n
                 .sdram_controller_0_wire_dq(SDRAM1_D),      //                            .dq
                 .sdram_controller_0_wire_dqm(SDRAM1_DQM),     //                            .dqm
                 .sdram_controller_0_wire_ras_n(SDRAM1_RAS),   //                            .ras_n
                 .sdram_controller_0_wire_we_n(SDRAM1_WE),
                   
                 .sdram_controller_0_s1_address({9'h0,sdram1Addr}),            //       sdram_controller_0_s1.address
                 .sdram_controller_0_s1_byteenable_n(2'b00),  //                            .byteenable_n
                 .sdram_controller_0_s1_chipselect(1'b1),         //                            .chipselect
                 .sdram_controller_0_s1_writedata(sdram1WrData),          //                            .writedata
                 .sdram_controller_0_s1_read_n(!sdram1RdReqW),             //                            .read_n
                 .sdram_controller_0_s1_write_n(!sdram1WrReqW),            //                            .write_n
                 .sdram_controller_0_s1_readdata(sdram1RdData),           //                            .readdata
                 .sdram_controller_0_s1_readdatavalid(sdram1RdValid),      //                            .readdatavalid
                 .sdram_controller_0_s1_waitrequest(sdram1WaitReq),


                 .sdram_controller_1_wire_addr(SDRAM2_A),    // new_sdram_controller_0_wire.addr
                 .sdram_controller_1_wire_ba(SDRAM2_BA),      //                            .ba
                 .sdram_controller_1_wire_cas_n(SDRAM2_CAS),   //                            .cas_n
                 .sdram_controller_1_wire_cke(SDRAM2_CKE),     //                            .cke
                 .sdram_controller_1_wire_cs_n(SDRAM2_CS),    //                            .cs_n
                 .sdram_controller_1_wire_dq(SDRAM2_D),      //                            .dq
                 .sdram_controller_1_wire_dqm(SDRAM2_DQM),     //                            .dqm
                 .sdram_controller_1_wire_ras_n(SDRAM2_RAS),   //                            .ras_n
                 .sdram_controller_1_wire_we_n(SDRAM2_WE),

                 .sdram_controller_1_s1_address(sdram2Addr),            //       sdram_controller_0_s1.address
                 .sdram_controller_1_s1_byteenable_n(2'b00),  //                            .byteenable_n
                 .sdram_controller_1_s1_chipselect(1'b1),         //                            .chipselect
                 .sdram_controller_1_s1_writedata(sdram2WrData),          //                            .writedata
                 .sdram_controller_1_s1_read_n(!sdram2RdReqW),             //                            .read_n
                 .sdram_controller_1_s1_write_n(!sdram2WrReqW),            //                            .write_n
                 .sdram_controller_1_s1_readdata(sdram2RdData),           //                            .readdata
                 .sdram_controller_1_s1_readdatavalid(sdram2RdValid),      //                            .readdatavalid
                 .sdram_controller_1_s1_waitrequest(sdram2WaitReq));


wire spDataValid, usbProtoGenReady, spSop;
wire [7:0] spData;

wire usbFifo88WrClk,  usbFifoWrReq, usbFifoFull /* synthesis noprune */;
wire [7:0] usbFifoData /* synthesis noprune */;

wire caruPresent;
wire carBusFifoWrFull;
wire [3:0] slavenLock;
wire slaveFpgaPresent;

wire [1:0] bb;


streamProcessor streamProcessorInst(.nrst(reset&(ID==0)), .clk(clkInt), 
                                    .stateReq(stateReq), .qBusReq(qBusReq), .eBusReq(eBusReq),
												.qBusData(qBusFifoOut), .qBusEmpty(qBUSfifoEmpty), .qBusRdReq(qBusFifoRdReq), .qBusUsedw(qBusUsedw), 
                                    .qBus_nLock(QB_nLOCK), .qBus_wrFull(qBUSfifoFull),
												.caruBusEmpty(1), .caruBusData(0), .caruBusPresent(caruPresent), .carBusWrFull(carBusFifoWrFull),
												.eBus1Data({2'h2,sdram1FifoQ}), .eBusCh1Usedw(eBusChUsedwIntClk[0]), //.eBus1ReadValid(sdram1RdValid), .sdram1WaitReq(sdram1WaitReq),
												.eBus2Data(sdram1RdData), .eBusCh2Usedw(eBusChUsedwIntClk[1]), .eBus2ReadValid(sdram1RdValid),
												.eBus3Data(sdram2RdData), .eBusCh3Usedw(eBusChUsedwIntClk[2]), .eBus3ReadValid(sdram2RdValid),
												.eBus4Data(sdram2RdData), .eBusCh4Usedw(eBusChUsedwIntClk[3]), .eBus4ReadValid(sdram2RdValid),
                                    .eBusCh5Usedw(eBusCh5Usedw),
                                    .eBusCh6Usedw(eBusCh6Usedw),
                                    .eBusCh7Usedw(eBusCh7Usedw),
                                    .eBusCh8Usedw(eBusCh8Usedw),
                                    .sdram1OutFifoData(sdram1OutFifoQ), .sdram1OutFifoEmpty(sdram1OutFifoE), .sdram1OutFifoRdReq(sdram1FifoRdReq),
                                    .sdram2OutFifoData(sdram2OutFifoQ), .sdram2OutFifoEmpty(sdram2OutFifoE), .sdram2OutFifoRdReq(sdram2FifoRdReq),
                                    /*.eBusRdReq({ebusFifoRdReq[7:4], bb}),*/ .eBusEmpty({ebusFifoEmpty[7:1], sdram1FifoE}),
                                    .eBus_nLock({slavenLock[3], slavenLock[2], slavenLock[1], slavenLock[0], /*4'hf,*/ CH4_nLock, CH3_nLock, CH2_nLock, CH1_nLock}),
                                    .eBusWrFull({4'hf, ebusFifoFullR[3:0]}), 
                                    .sdram1DataCount({3'h0,sdram1Addr}),    //.sdram1DataCount({3'h0,27'hffbaba}), 
                                    .sdram2DataCount({3'h0,sdram2Addr}),    //.sdram2DataCount({3'h0,27'hffbbba}),  //.sdram2DataCount({3'h0,sdram2Addr}), 
                                    .sdram3DataCount(0),    //.sdram3DataCount({3'h0,27'hffbcba}), 
                                    .sdram4DataCount(0),   //.sdram4DataCount({3'h0,27'hffbdba}),
                                    .sdramFull({1'b0, 1'b0, sdram2Full,sdram1Full}),
                                    .ad9510DataReady(0 /*ad9510DataReady*/), .ad9510DataRdAck(ad9510DataRdAck),  .ad9510Data(ad9510Data),
                                    .slaveFpgaPresent(slaveFpgaPresent),
                                    .TIM1Locked(TIM1Locked), .TIM2Locked(TIM2Locked),
                                    .ready(!usbFifoFull), .dataValid(usbFifoWrReq), .q(usbFifoData), .sop(spSop));
                                    //.ready(usbProtoGenReady), .dataValid(spDataValid), .q(spData), .sop(spSop)); 



/*UsbProtoGenerator UsbProtoGeneratorInst(.nrst(reset), .clk(clk168),
                  .inValid(spDataValid), .inReady(usbProtoGenReady), .sop(spSop), .data(spData),
                  .fifoWrClk(usbFifo88WrClk), .outReady(!usbFifoFull), .outValid(usbFifoWrReq), .q(usbFifoData));*/

wire SLAVE_CYC_MISO1, SLAVE_CYC_MISO2, SLAVE_CYC_MISO3;
        
reg fifoUsbTxRdReq = 0;
reg [3:0] lbFifoRdReqD = 4'h0;
//wire fifoUsbTxRdReqW = sendToUsbWrAllow&&~fifoUsbTxRdEmpty;


//reg usbFifo88WrReq=0;
reg [1:0] usbFtnTxeD=1; always @(posedge ftClk) usbFtnTxeD <= {usbFtnTxeD[0],USB_FT_nTXE};
wire sendToUsbEmpty;
wire usbTransmitAllow = (USB_FT_nOE==1)&&(USB_FT_nTXE == 0)/*&&(nTxeD[2] == 0)&&(fifoUsbTxRdEmptyR[7]==0)&& (fifoUsbTxRdEmptyR[1]==0)*/;
//wire usbFifo88RdReq = !FtnWr;
wire usbFifo88RdReq = usbTransmitAllow &&(usbFtnTxeD[0]==0) && ~sendToUsbEmpty;
assign usbFifo88RdReqPn = usbFifo88RdReq;

wire [7:0] usbTxOut;

usbFifo88DualClk usbTxFifo88(.wrclk(clkInt),.data(usbFifoData), .wrreq(usbFifoWrReq), .wrfull(usbFifoFull),
                                      .rdclk(ftClk), .rdempty(sendToUsbEmpty), .rdreq(usbFifo88RdReq), .q(usbTxOut));

//wire fifoUsbTxWrFull;
//wire strProcRd = ~strProcEmpty&&~fifoUsbTxWrFull;
//reg [23:0] sendToUsbCounter=0; always @(posedge CLOCK_56) if(~fifoUsbTxWrFull) sendToUsbCounter<=sendToUsbCounter+1;

reg [7:0] fifoUsbTxRdEmptyR=0; always @(posedge ftClk) fifoUsbTxRdEmptyR <= {fifoUsbTxRdEmptyR[6:0], fifoUsbTxRdEmpty};

reg [2:0] nTxeD=3'd1; always @(posedge ftClk) nTxeD <= {nTxeD[1:0], USB_FT_nTXE};

reg FtnWr; always @(posedge ftClk) FtnWr <= ~(usbTransmitAllow && ~sendToUsbEmpty); // ~usbFifo88RdReq;
assign USB_FT_nWR = FtnWr;
assign USB_FT_D = (ID==0)? (USB_FT_nRXF ?  usbTxOut : 8'bz) :
                  {6'h0, SLAVE_CYC_MISO1, SLAVE_CYC_MISO2, SLAVE_CYC_MISO3};
//reg readValid = 0;
//assign USB_FT_nWR = !(readValid && (fifoUsbTxRdEmpty==0));
//reg [1:0] nWr=1; always @(posedge USB_FT_CLKIN) nWr<= { nWr[0], transmitAllow};
//assign USB_FT_nWR = !(nWr[1] /*&& (lbFifoRdReqD!=0)*/  );

assign lbFifoRdReqPin = fifoUsbTxRdReq;
 

reg QB_RClkD=0; always @(posedge CLOCK_56) QB_RClkD <= QB_RCLK;
//wire QB_ReadValid = ((QB_ROUT[15:0]!=16'h1111) && (QB_ROUT[15:0]!=16'h0000) && (QB_ROUT[15:0]!=16'h01ff) && 
//							(fifoUsbTxWrFull==0) && (QB_RCLK==1)&&(QB_RClkD==0));

reg [1:0] usbRecvState = 0; //idle

reg [15:0] fifoWrCnt = 0;


//reg [17:0] dataIn;
//reg [8:0] rClkCount = 0;
//always @(posedge CLOCK_56) begin
	//if(QB_RCLK)
		//dataIn <= QB_ROUT;		
	//rClkCount <= rClkCount + 1;
//end



						
reg [31:0] ch1ClkCnt = 0;
always @(posedge CH1_RCLK) begin
	//dataIn <= CH1_RE;
	//if(ch12WrReq) begin
	//	ch1ClkCnt <= ch1ClkCnt + 1;
	//end	
end

always @(posedge CH2_RCLK) begin
	//dataIn <= CH1_RE;
	//if(ch12WrReq)
	//	ch2ClkCnt <= ch2ClkCnt + 1;
end


//assign CARU_C_OUT = 1;
//assign CARU_D_OUT = 1;

//assign CARU_C_OUT = clkCnt[10];
//assign CARU_D_OUT = clkCnt[11];
/*caru caru_inst(.reset(reset),.clk(CLOCK_56), .caruClkIn(CARU_C_IN), .caruDataIn(CARU_D_IN),
				//.caruMem(caruMem),
				.caruRamRdAddr(caruRamRdAddr), .caruRamQ(caruRamQ),
				.caruClkOut(CARU_C_OUT), .caruDataOut(CARU_D_OUT),
				.debCaruDataByte(debCaruDataByte));
				*/

wire [7:0] caruRdData;
wire caruWrReq, caruWrFull;            
wire [7:0] caruRdQ;
wire caruRdReq;

/* caru  caru_inst(.reset(reset), .clk(clkInt), .caruClkIn(CARU_C_IN), /*.caruDataIn(CARU_D_IN),
               .caruClkOut(CARU_C_OUT), .caruDataOut(CARU_D_OUT), .present(caruPresent),
               .wrReq(), .data(), .wrFull(carBusFifoWrFull)); */
				/*output reg [3:0] caruRamRdAddr=0, input wire [7:0] caruRamQ,
				output [7:0] debCaruDataByte*/
//caru_fifo caruFifo(.wrclk(CARU_C_IN), .wrreq(caruWrReq), .wrfull(caruWrFull), .data(caruRdData), .rdclk(clk168), .rdreq(), .rdempty(), .q(caruRdQ));
				
wire ready;
reg wr_req;

	
	

// Declare state register
reg		[1:0]state = 2'b00;

//if() USB_FT_nTXE

//	SFL sfl();
	
	
//assign CARU_EN = 0;

reg [7:0] caruRamData;
//wire [7:0]caruRamQ;
reg [3:0] caruRamWrAddr;
//wire [3:0] caruRamRdAddr;
reg caruRamWrEn = 0;
//caru_ram	caru_ram_inst (
//	.data ( caruRamData ),
//	.wrclock ( clk28 ),
//	.rdclock ( CLOCK_56 ),
//	.rdaddress ( caruRamRdAddr ),
//	.wraddress ( caruRamWrAddr ),
//	.wren ( caruRamWrEn ),
//	.q ( caruRamQ )
//	);
   

wire clockQspi;
reg [32:0] CLOCK_2MHZ_cnt=0;
always @(posedge CLOCK_2MHZ) begin
   CLOCK_2MHZ_cnt <= CLOCK_2MHZ_cnt + 1;
end
assign clockQspi = CLOCK_2MHZ_cnt[4];

// CYCIV  
wire spiSckM, spiSsM, spiMosiM;
wire spiMisoS;
  
assign CYC_SPI_SCK = (ID==0) ? spiSckM : 1'hZ;
assign CYC_SPI_MISO = (ID==0) ? 4'hZ : {3'hZ, spiMisoS};
assign CYC_SPI_MOSI = (ID==0) ? spiMosiM : 1'hZ;

assign CYC_SPI_SS = (ID==0) ? spiSsM : 1'hZ;



QSPI_Master spiMaster(.nrst(reset&isMaster), .clk(clk28), 
                       .eBusReq(eBusReq[0]),
                       .sck(spiSckM), .miso(CYC_SPI_MISO), .mosi(spiMosiM), .ss(spiSsM), 
                       .nLock(slavenLock), .slavePresent(slaveFpgaPresent),
                       .ebusFifoFull(ebusFifoFull[7:4]),
                       .eBusCh1Usedw(eBusCh5Usedw), .eBusCh2Usedw(eBusCh6Usedw), .eBusCh3Usedw(eBusCh7Usedw), .eBusCh4Usedw(eBusCh8Usedw));
                       
QSPI_Slave spiSlave(.nrst(reset&isSlave), .sck(CYC_SPI_SCK), .miso(/*spiMisoS*/ {SLAVE_CYC_MISO3, SLAVE_CYC_MISO2, SLAVE_CYC_MISO1, spiMisoS}), .mosi(CYC_SPI_MOSI), .ss(CYC_SPI_SS), 
                     .nLockBus({CH4_nLock, CH3_nLock, CH2_nLock, CH1_nLock}),
                     .ebusFifoFull(ebusFifoFull[3:0]),
                     .eBusCh1Usedw(eBusCh1Usedw), .eBusCh2Usedw(eBusCh2Usedw), .eBusCh3Usedw(eBusCh3Usedw), .eBusCh4Usedw(eBusCh4Usedw));





endmodule

