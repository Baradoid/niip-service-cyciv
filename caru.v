module caru(input wire reset, 
				input wire clk, 
				input wire caruClkIn, caruDataIn,
            output caruClkOut, caruDataOut,
				output reg [3:0] caruRamRdAddr=0, input wire [7:0] caruRamQ,
				output [7:0] debCaruDataByte,
            output reg present=0,
            output reg wrReq=0, output reg [7:0] data=0,
            output wrFull);

reg [1:0] state = 0; 

reg [2:0] symbCount = 0;
reg posEdgeState = 0; always @(posedge caruClkIn) posEdgeState <= caruDataIn;
wire [1:0] dataSymbol = {posEdgeState, caruDataIn};

reg allowTranslateClock = 0;

//assign caruClkOut = allowTranslateClock? caruClkIn : 0;
assign caruClkOut = clk;

reg [1:0] memInd = 0;

//reg [1:0] caruDataSymbOutArrOut[0:6*8];
reg [7:0] caruDataOutArr[0:6];
reg [7:0] curDataByte;
assign debCaruDataByte = curDataByte;
reg [1:0] caruDataSymbOut=0;
reg [7:0] arrIdx = 0;
reg [3:0] bitIdx = 0;
assign caruDataOut=allowTranslateClock? (caruClkIn? caruDataSymbOut[1]: caruDataSymbOut[0]): 0;

reg [7:0] clkCounter = 0;
initial begin
   caruDataOutArr[0] = 8'h00;
   caruDataOutArr[1] = 8'h00;
   caruDataOutArr[2] = 8'h00;
   caruDataOutArr[3] = 8'h00;
   caruDataOutArr[4] = 8'h00;
   caruDataOutArr[5] = 8'hff;

end

wire datain_h, datain_l;
ddio_in ddio_in_inst(.aclr(reset), .datain(caruDataIn), .inclock(caruClkIn), .dataout_h(datain_h), .dataout_l(datain_l));



always @(posedge caruClkIn) begin
   clkCounter <= clkCounter + 1;
   
	if(state == 0) begin // wait for start
		
      allowTranslateClock <= (dataSymbol == 2'b11) && (symbCount>1);
      
		if(dataSymbol == 2'b00) begin   //start detected
			caruRamRdAddr <= 0;
		
			symbCount <= symbCount + 1;
		end
		else if((dataSymbol == 2'b11) && (symbCount>1)) begin //stop detected
			caruRamRdAddr <= caruRamRdAddr + 1;
			
			state <= 1;      			//step to send frame
			symbCount <= 4 ;			//send 4 start
			caruDataSymbOut <= 2'b00;
		end
		else begin
			symbCount <= 0;
		end
	end
	else if(state == 1) begin // send start
		
      curDataByte<= caruRamQ;         

		if(symbCount == 0) begin 
			caruRamRdAddr <= caruRamRdAddr + 1;
			
			state <=2;
         arrIdx <= 1;
         bitIdx <= 7;			
		end
		else begin
			symbCount <= symbCount - 1;
		end
		
	end
	else if(state == 2) begin // send frame
      caruDataSymbOut <= curDataByte[7]? 2'b01 : 2'b10;

		caruRamRdAddr <= caruRamRdAddr + 1;
		
      if(bitIdx == 0) begin         
			curDataByte <= caruRamQ;         
			bitIdx <= 7;         			
			if(arrIdx==6) begin 
				state <=3;   //send stop
				symbCount <= 0;         
         
			end
			else begin
				arrIdx <= arrIdx + 1;
			end
         
      end
      else begin
         bitIdx <= bitIdx - 1;
         curDataByte <= {curDataByte[6:0], 1'b0};
      end

	end
   else if(state == 3) begin  // send stop
      caruDataSymbOut <= 2'b11;
      state <=0;   // wait for next start
   end

end

reg [7:0] lastClkCounter = 0;
reg [31:0] presentTimeOut = 0;
always  @(posedge clk) begin
   if(clkCounter != lastClkCounter) begin
      lastClkCounter <= clkCounter;   
      presentTimeOut <= 5600000; // 1/10 sec
   end
   else begin
      if(presentTimeOut>0)  presentTimeOut <= presentTimeOut - 1;
   end
   present <= (presentTimeOut>0);
   
end

endmodule
