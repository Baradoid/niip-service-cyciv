module sendToUsb(input clk, input [23:0] data, input wrReq, input rdReq, 
						output [7:0] q, output wrAllow, output empty, output [23:0] debugDataR);

reg [23:0] dataIn;					
reg [23:0] dataOut; 
assign debugDataR = dataOut;
reg inputRegFree = 1;

reg [1:0] outByteCount=0;
always @(posedge clk) begin
	if(wrReq && (inputRegFree==1)) begin
		dataIn <= data;
		inputRegFree <= 0;
	end
	if(((outByteCount==0)||(outByteCount==1))&&(inputRegFree==0)) begin
		dataOut <= dataIn;
		outByteCount <= 3'h3;
		inputRegFree <= 1;	
	end
	else if(rdReq && (outByteCount>0)) begin
		outByteCount <= outByteCount - 2'h1;
		dataOut[23:0] <= {dataOut[15:0], 8'h00};	
	end
end

assign empty = ((outByteCount==0)||(outByteCount==1))&&inputRegFree;
assign wrAllow = inputRegFree;
assign q = dataOut[23:16];

						

endmodule


module recvFromUsb(input clk, input [7:0] data, input wrReq, input rdReq, 
						output [23:0] q, output wrAllow, output empty, output [23:0] debugDataR);
endmodule
