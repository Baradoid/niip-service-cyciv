

module streamProcessor(input wire nrst, input wire clk,
            input wire stateReq, qBusReq, 
            input wire [7:0] eBusReq,
            
				input wire [17:0] qBusData,
				input wire qBusEmpty,
            input wire [9:0] qBusUsedw,
				output qBusRdReq,
            input wire qBus_nLock,
            input wire qBus_wrFull,

				input wire [17:0] caruBusData,
				input wire caruBusEmpty, caruBusPresent,
				output caruBusRdReq,
            input carBusWrFull,
				
            input [7:0] eBus_nLock,
            input [3:0] eBusEmpty,
            input [7:0] eBusWrFull,
            //output [7:0] eBusRdReq,
            
				input wire [17:0] eBus1Data,
            input wire [15:0] eBusCh1Usedw,
            //input wire eBus1ReadValid,
            //input wire sdram1WaitReq,
				
            
				input wire [17:0] eBus2Data,
            input wire [15:0] eBusCh2Usedw,
            input wire eBus2ReadValid,

				input wire [17:0] eBus3Data,
            input wire [15:0] eBusCh3Usedw,
            input wire eBus3ReadValid,
				
				input wire [17:0] eBus4Data,
            input wire [15:0] eBusCh4Usedw,
            input wire eBus4ReadValid,
            
            input wire [7:0] sdram1OutFifoData,
            input wire sdram1OutFifoEmpty,
            output sdram1OutFifoRdReq,

            input wire [7:0] sdram2OutFifoData,
            input wire sdram2OutFifoEmpty,
            output sdram2OutFifoRdReq,

            input wire [15:0] eBusCh5Usedw,
            input wire [15:0] eBusCh6Usedw,
            input wire [15:0] eBusCh7Usedw,
            input wire [15:0] eBusCh8Usedw,
            input wire [3:0]  sdramFull,
            input wire [29:0] sdram1DataCount,
            input wire [29:0] sdram2DataCount,
            input wire [29:0] sdram3DataCount,
            input wire [29:0] sdram4DataCount,

				input wire [7:0] ad9510Data,
				input wire ad9510DataReady,
				output reg ad9510DataRdAck,
            
            input slaveFpgaPresent,
            
            input TIM1Locked,
            input TIM2Locked,
				
				input rdReq, 
            output empty,
				output [23:0] out,
				
				input ready,
				output dataValid,
				output [7:0] q,
            output reg sop=0);
	//include 'CRC16_D8.v'
	
reg [15:0] dataBuf; 
reg [1:0] outByteCount=0;

localparam [4:0] IDLE = 4'h0,
                QBUS = 4'h1,
                CARU = 4'h2,
                EBUS1 = 4'h3,
                EBUS2 = 4'h4,
                EBUS3 = 4'h5,
                EBUS4 = 4'h6,
                EBUS5 = 4'h7,
                EBUS6 = 4'h8,
                EBUS7 = 4'h9,
                EBUS8 = 4'h10,
                AD9510 = 4'h11,
                STATE_REQ = 4'h12;
                
reg [4:0] state = IDLE; //0-idle,1-qbus,2-caru,3-ebus

//localparam [26:0] delayVal = 28'h7ffffff; //0.5s on 168 MHz
//localparam [26:0] delayVal = 28'h7; //0.5s on 168 MHz
//parameter [26:0] delayVal = 100; //0.5s on 168 MHz
//reg [26:0] dataDelay=0;
//reg [3:0] dataDelayR; always @(posedge clk) dataDelayR<= {dataDelayR[2:0], (dataDelay==0)};
reg [50:0] dataSendState=0;

reg [5:0] qBusEmptyDDD = 0;

reg [8:0] qBusUsedwR;
reg [7:0] qBusUsedwNotChangedCnt = 0;
localparam [3:0] qBusDataPack = 4;
//wire qBusReadyToRead = (qBusUsedwNotChangedCnt>5) || (qBusUsedw>=qBusDataPack);

wire b3bto1bReady;
//reg qBusRdReqRR = 0;
//reg qBusRdReqR = 0;
reg [31:0] dataLen = 0;
//reg [31:0] dataLenInBytes = 0;
wire rdReqW = ready && b3bto1bReady  && dataSendState[8:7] && (dataLen>0);
assign qBusRdReq = rdReqW && !qBusEmpty && (state == QBUS);
reg qBusDataValid = 0; always @(posedge clk) qBusDataValid <= qBusRdReq;
reg [2:0] ebusInd =0;

//assign eBusRdReq[0] = rdReqW && !(eBusEmpty[ebusInd]) && (state == EBUS1);
//reg eBus1DataValid = 0; //always @(posedge clk) eBus1DataValid <= eBusRdReq[0];

//assign eBusRdReq[1] = rdReqW && !(eBusEmpty[ebusInd]) && (state == EBUS2);
//assign eBusRdReq[2] = rdReqW && !(eBusEmpty[ebusInd]) && (state == EBUS3);
//assign eBusRdReq[3] = rdReqW && !(eBusEmpty[ebusInd]) && (state == EBUS4);
//assign eBusRdReq[7:4] = 0;

//reg eBusDataValid = 0; always @(posedge clk) eBusDataValid <= eBusRdReq[ebusInd];

assign sdram1OutFifoRdReq = ready && (dataSendState[8:7]!=0) && (dataLen>0) && !sdram1OutFifoEmpty && (state == EBUS1);
reg sdram1OutFifoDataValid=0; always @(posedge clk) if(ready) sdram1OutFifoDataValid <= sdram1OutFifoRdReq;

assign sdram2OutFifoRdReq = ready && (dataSendState[8:7]!=0) && (dataLen>0) && !sdram2OutFifoEmpty && (state == EBUS2);
reg sdram2OutFifoDataValid=0; always @(posedge clk) if(ready) sdram2OutFifoDataValid <= sdram2OutFifoRdReq;


//reg qBusDataValidR = 0; always @(posedge clk)  qBusDataValidR <= (qBusRdReq || qBusDataValidR) && !qBusEmpty;

always @(posedge clk) begin
   qBusUsedwR<=qBusUsedw;
   if((qBusUsedwR == qBusUsedw) && (qBusUsedw !=0)) begin
      qBusUsedwNotChangedCnt <= qBusUsedwNotChangedCnt + 1;
   end
   else begin
      qBusUsedwNotChangedCnt <= 0;
   end
end

   

reg [2:0] eBus1EmptyDDD = 0;
reg [2:0] eBus2EmptyDDD = 0;

reg emptyR;


reg usb3bto1bWrReq = 0;
wire [7:0] usb3to1DataOut;



wire b3to1DataValid;
//b3bto1b b3bto1b_inst(.reset(nrst), .clk(clk), .data({6'h2d,qBusData}), .wrReq(qBusDataValid), 
wire [23:0] b3b1Data = (state==QBUS)? {6'h00,qBusData} :
                       (state==EBUS1)? {6'h00,eBus1Data} :
                       (state==EBUS2)? {6'h00,eBus2Data} :
                       (state==EBUS3)? {6'h00,eBus3Data} :
                       (state==EBUS4)? {6'h00,eBus4Data} :
                        24'h0;


wire b3b1WrReq = (state==QBUS) ? qBusDataValid : 0 /*eBus1DataValid*/;

b3bto1b b3bto1b_inst(.nrst(nrst), .clk(clk), .data(b3b1Data), .wrReq(b3b1WrReq), 
							.q(usb3to1DataOut), .ready(b3bto1bReady), .dataValid(b3to1DataValid), .outReady(ready));


//assign dataValid = qBusDataValidR || (sop==1) ;
//assign data = dataValid ? qBusData : 17'h3ffff;
assign dataValid = state==IDLE ?  0/*(dataSendState==1) || (((dataSendState==3)||(dataSendState==2))&&ready)*/ :
                  (state==STATE_REQ) ? (dataSendState>1) :
                  (state==EBUS1)? ready&&(dataSendState[7:1]||sdram1OutFifoDataValid) :
                  (state==EBUS2)? ready&&(dataSendState[7:1]||sdram2OutFifoDataValid) :
                  ((state==QBUS)||(state==EBUS2)||(state==EBUS3)||(state==EBUS4)) ? (dataSendState[7:1]||b3to1DataValid) :  //|| (dataSendState[5]&&b3to1DataValid)*/ 
                                                     0;

reg [7:0] dataR = 0;
assign q = ((state==IDLE)||(state==STATE_REQ)) ? dataR :  
           (state==EBUS1) ? ((dataSendState[7:0]!=0)? dataR: sdram1OutFifoData) :
           (state==EBUS2) ? ((dataSendState[7:0]!=0)? dataR: sdram2OutFifoData) :
           ((state==QBUS)||(state==EBUS2)||(state==EBUS3)||(state==EBUS4)) ? (dataSendState[7:0])? dataR:usb3to1DataOut : 
                                                18'h0000;

assign empty = 1;
/* wire [23:0] dataW = (dataCnt==3)?24'heeeeee:
					(dataCnt==2)?24'heeeeee:
               (dataCnt==1)?24'hee1122:
               (dataCnt==0)?24'hee3344:24'heeeeee;

wire emptyLoc = (dataDelay==0)? ((dataCnt==3)?0:
											(dataCnt==2)?0:
                                 (dataCnt==1)?0:
                                 (dataCnt==0)?1:0):1; */


reg stateReqR = 0; always @(posedge clk) stateReqR <= (state==STATE_REQ)? 0 : stateReq|stateReqR;
reg qbusReqR = 0; always @(posedge clk) qbusReqR <= (state==QBUS)? 0 : qBusReq|qbusReqR;

integer i;
reg [7:0] ebusReqR = 0; 
always @(posedge clk) begin
   for(i=0; i<8; i=i+1) begin
      ebusReqR[i] <= (state==EBUS1+i)? 0 : eBusReq[i] | ebusReqR[i];
   end
end

//reg [7:0] eBusWrFullR=0; always @(posedge clk)  eBusWrFullR <= eBusWrFull;

assign fifoWrClk = clk;
always @(posedge clk) begin
   qBusEmptyDDD <= {qBusEmptyDDD[4:0], !qBusEmpty};
   //eBus1EmptyDDD <= {eBus1EmptyDDD[2:0], !eBus1Empty};
	//eBus2EmptyDDD <= {eBus2EmptyDDD[2:0], !eBus2Empty};
   
   
   if(!nrst) begin
      state <= IDLE;
      //dataDelay <= 27'h7FFFFFF;
      //dataDelay <= delayVal;
      dataSendState <= 0;
      sop <= 0;
      dataLen <= 0;
      dataSendState <= 0;
   end
   else begin   
/*       if(ready) begin
         dataSendState <= dataSendState + 1;
      end   */   
      if(sop == 1) sop <= 0;            
      if(state == IDLE) begin
         dataSendState <= 1;
         if(stateReqR) begin
            state <= STATE_REQ;
            
            //dataLen <= 4+9;
         end
         else if(qbusReqR/*qBusReadyToRead || caruBusEmpty || eBus1Empty || eBus2Empty || eBus3Empty || eBus4Empty*/) begin
            state <= QBUS;
            dataLen <=  qBusUsedw*3+1;
         end 
         else if(ebusReqR[0] /*ad9510DataReady*/) begin
            state <= EBUS1;
            dataLen <=  sdram1DataCount*2+1;
            //dataLenInBytes <= eBusCh1Usedw*3+1;
            ebusInd <= 0;
         end
         else if(ebusReqR[1] /*ad9510DataReady*/) begin
            state <= EBUS2;
            dataLen <= sdram2DataCount*2+1;
            //dataLenInBytes <= eBusCh2Usedw*3+1;
            ebusInd <= 1;
         end
         else if(ebusReqR[2] /*ad9510DataReady*/) begin
            state <= EBUS3;
            dataLen <= eBusCh3Usedw*3+1;
            //dataLenInBytes <= eBusCh3Usedw*3+1;
            ebusInd <= 2;
         end
         else if(ebusReqR[3] /*ad9510DataReady*/) begin
            state <= EBUS4;
            dataLen <= eBusCh4Usedw*3+1;
            //dataLenInBytes <= eBusCh4Usedw*3+1;
            ebusInd <= 3;
         end

         //else if(0 /*ad9510DataReady*/) begin
         //   state <= AD9510;      
         //end
         //else begin
               //dataDelay <= dataDelay-1;
               //dataSendState <= 0;
         //end

      end
      else if(state == STATE_REQ) begin
         if(ready) begin
            dataSendState <= {dataSendState[49:0], 1'b0};
            if(dataSendState[0]) dataR <= 8'hbe;
            if(dataSendState[1]) dataR <= 8'hef;
            if(dataSendState[2]) dataR <= 0;
            if(dataSendState[3]) dataR <= 0;
            if(dataSendState[4]) dataR <= 0;
            if(dataSendState[5]) dataR <= 39;
            if(dataSendState[6]) dataR <= 8'hcc;
            if(dataSendState[7]) dataR <= {2'h0, caruBusPresent, 1'b0, TIM2Locked, TIM1Locked, slaveFpgaPresent, qBus_nLock};
            if(dataSendState[8]) dataR <= eBus_nLock;
            if(dataSendState[9]) dataR <= eBusWrFull;
            if(dataSendState[10]) dataR <= {2'h5, carBusWrFull,   4'h0, qBus_wrFull};
            if(dataSendState[11]) dataR <= {6'd0, qBusUsedw[9:8]};
            if(dataSendState[12]) dataR <= qBusUsedw[7:0];
            if(dataSendState[13]) dataR <= eBusCh1Usedw[15:8];
            if(dataSendState[14]) dataR <= eBusCh1Usedw[7:0];
            if(dataSendState[15]) dataR <= eBusCh2Usedw[15:8];
            if(dataSendState[16]) dataR <= eBusCh2Usedw[7:0];
            if(dataSendState[17]) dataR <= eBusCh3Usedw[15:8];
            if(dataSendState[18]) dataR <= eBusCh3Usedw[7:0];
            if(dataSendState[19]) dataR <= eBusCh4Usedw[15:8];
            if(dataSendState[20]) dataR <= eBusCh4Usedw[7:0];
            
            if(dataSendState[21]) dataR <= eBusCh5Usedw[15:8];
            if(dataSendState[22]) dataR <= eBusCh5Usedw[7:0];
            if(dataSendState[23]) dataR <= eBusCh6Usedw[15:8];
            if(dataSendState[24]) dataR <= eBusCh6Usedw[7:0];
            if(dataSendState[25]) dataR <= eBusCh7Usedw[15:8];
            if(dataSendState[26]) dataR <= eBusCh7Usedw[7:0];
            if(dataSendState[27]) dataR <= eBusCh8Usedw[15:8];
            if(dataSendState[28]) dataR <= eBusCh8Usedw[7:0];
            
            if(dataSendState[29]) dataR <= {sdramFull[0], 1'b0, sdram1DataCount[29:24]};
            if(dataSendState[30]) dataR <= sdram1DataCount[23:16];
            if(dataSendState[31]) dataR <= sdram1DataCount[15:8];
            if(dataSendState[32]) dataR <= sdram1DataCount[7:0];

            if(dataSendState[33]) dataR <= {sdramFull[1], 1'b0, sdram2DataCount[29:24]};
            if(dataSendState[34]) dataR <= sdram2DataCount[23:16];
            if(dataSendState[35]) dataR <= sdram2DataCount[15:8];
            if(dataSendState[36]) dataR <= sdram2DataCount[7:0];

            if(dataSendState[37]) dataR <= {sdramFull[2], 1'b0, sdram3DataCount[29:24]};
            if(dataSendState[38]) dataR <= sdram3DataCount[23:16];
            if(dataSendState[39]) dataR <= sdram3DataCount[15:8];
            if(dataSendState[40]) dataR <= sdram3DataCount[7:0];

            if(dataSendState[41]) dataR <= {sdramFull[3], 1'b0, sdram4DataCount[29:24]};
            if(dataSendState[42]) dataR <= sdram4DataCount[23:16];
            if(dataSendState[43]) dataR <= sdram4DataCount[15:8];
            if(dataSendState[44]) dataR <= sdram4DataCount[7:0];
            
            if(dataSendState[45]) state <= IDLE;
         end
      end
      
      else if(state == QBUS) begin
         if(ready) begin
            if(!(dataSendState[8]&&(dataLen>0))) dataSendState <= {dataSendState[49:0], 1'b0};
            
            if(dataSendState[0]) dataR <= 8'hbe;
            if(dataSendState[1]) dataR <= 8'hef;
            if(dataSendState[2]) dataR <= dataLen[31:24];
            if(dataSendState[3]) dataR <= dataLen[23:16];
            if(dataSendState[4]) dataR <= dataLen[15:8];
            if(dataSendState[5]) dataR <= dataLen[7:0];
            if(dataSendState[6]) dataR <= 8'hbb;
            if(dataSendState[8:7]) if(dataValid) dataLen <= dataLen - 1;
            if(dataSendState[9]) state <= IDLE;
         end         
      end
         
      else if(state == CARU) begin
      end
         
      else if((state == EBUS1)||(state == EBUS2)||(state == EBUS3)||(state == EBUS4)) begin
         if(ready) begin
            if(!(dataSendState[8]&&(dataLen>0))) dataSendState <= {dataSendState[49:0], 1'b0};
            
            if(dataSendState[0]) dataR <= 8'hbe;
            if(dataSendState[1]) dataR <= 8'hef;
            if(dataSendState[2]) dataR <= dataLen[31:24];
            if(dataSendState[3]) dataR <= dataLen[23:16];
            if(dataSendState[4]) dataR <= dataLen[15:8];
            if(dataSendState[5]) dataR <= dataLen[7:0];
            if(dataSendState[6]) dataR <= {4'he, state[3:0]-4'h3}; // 8'hee;
            //(dataSendState[8:7]) if(dataValid) dataLen <= dataLen - 1;
            if(dataSendState[9]) state <= IDLE;
         end  
         if(dataSendState[8:7]) if(dataValid) dataLen <= dataLen - 1;
      end
         
/*       else if(state == EBUS2) begin
         if(eBusEmpty[1] == 0) begin
            state <= IDLE;
         end
         else begin
         end
      end
      
      else if(state == EBUS3) begin
         if(eBusEmpty[2] == 0) begin
            state <= IDLE;
         end
         else begin
         end
      end
      
      else if(state == EBUS4) begin
         if(eBusEmpty[3] == 0) begin
            state <= IDLE;
         end
         else begin
         end
      end */
         
      else if(state == AD9510) begin
         if(ad9510DataReady) begin
            ad9510DataRdAck<= 1;
            
         end
         else begin
            ad9510DataRdAck<= 0;
            state <= IDLE;
         end
      end
   end


end


/* assign empty=(state==0)?emptyLoc:
               (state==1)?qBusEmpty:
               (state==2)?caruBusEmpty:
					(state==3)?eBus1Empty:
               (state==4)?eBus2Empty:
					(state==5)?eBus3Empty:
					(state==6)?eBus4Empty:1; */

//assign qBusRdReqR   = (state==1)?rdReq:0;
/* assign caruBusRdReq = (state==2)?rdReq:0;
assign eBus1RdReq    = (state==3)?rdReq:0;
assign eBus2RdReq    = (state==4)?rdReq:0;
assign eBus3RdReq    = (state==5)?rdReq:0;
assign eBus4RdReq    = (state==6)?rdReq:0; */

/* assign out = (state==0)? dataW:
               (state==1)?{4'ha, 2'h0, qBusData[17:0]}:
               (state==2)?{4'hc, 2'h0, caruBusData[17:0]}:
					(state==3)?{4'hd, 2'h0, eBus1Data[17:0]}:
               (state==4)?{4'he, 2'h0, eBus2Data[17:0]}:
					(state==5)?{4'hb, 2'h0, eBus3Data[17:0]}:
					(state==6)?{4'hf, 2'h0, eBus4Data[17:0]}:24'hfff; */
               

//assign out = ;
//assign empty = ((outByteCount==0)||(outByteCount==1))&&inputRegFree;
//assign wrAllow = (outByteCount==1)||(outByteCount==0);
//assign q = (outByteCount==0)?data[7:0]:dataBuf[7:0];				


  
endmodule

