module b3bto1b(input wire reset, 
				input wire clk, 
				input wire [23:0] data, 
				input wrReq, 
				output [7:0] q, 
				output ready,
				output dataValid);

reg [15:0] dataBuf; 

reg [1:0] outByteCount=0;
always @(posedge clk) begin
	if((outByteCount==0)&&wrReq) begin
		dataBuf <= data[23:8];		
		outByteCount <=2;
	end
	else if(outByteCount>0) begin
		outByteCount <= outByteCount-1;
		dataBuf <= {8'h0, dataBuf[15:8]};
	end

end

//assign empty = ((outByteCount==0)||(outByteCount==1))&&inputRegFree;
assign ready = (outByteCount==1)||((outByteCount==0)&&!wrReq);
assign q = (outByteCount==0)?data[7:0]:dataBuf[7:0];				
assign dataValid = (outByteCount>0)||wrReq;

endmodule
