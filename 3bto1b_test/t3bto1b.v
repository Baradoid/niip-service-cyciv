`timescale 1 ns/ 1 ns
module test_3bto1b;

reg reset, clk;
reg [23:0]wdata;
wire [23:0] debugData;
wire [7:0] dataOut;

reg wrReq;
wire rdReq = (wrAllow&(~wrReq));
wire wrAllow, dataValid;

//устанавливаем экземпл¤р тестируемого модул¤
usb3bto1b usb3bto1b_inst(reset, clk, wdata, wrReq, dataOut, wrAllow, dataValid);

//моделируем сигнал тактовой частоты
always
  #10 clk = ~clk;

//от начала времени...

initial
begin
  clk = 0;
  reset = 1;
  wdata = 24'h223344;
  wrReq = 1'b0;
  //rdReq = 1'b0;

	repeat(1) begin
		@(posedge clk);
	end
  reset = 0;

//пауза длительностью "50"
  #10
	forever begin
		repeat(10) begin
			@(posedge clk) 
			begin
				//#1
				//rdReq <= (wrAllow&(~rdReq));
				wrReq <= rdReq;
				if(rdReq==1) wdata <= wdata+1;    				
			end
			//@(posedge clk)
			//wrReq <= rdReq;

			//@(posedge clk)		
			//begin
			//  wrReq <= rdReq;
			//  wdata <= wdata+1;    
			//end
		end
	#100;
	end
end

//заканчиваем симул¤цию в момент времени "400"
initial
begin
  #1000 $finish;
end

//создаем файл VCD дл¤ последующего анализа сигналов
initial
begin
  $dumpfile("out.vcd");
  $dumpvars(0,test_3bto1b);
end

//наблюдаем на некоторыми сигналами системы
initial
$monitor($stime,, reset,, clk,,, wdata,, wrReq,, dataOut,, wrAllow);

endmodule